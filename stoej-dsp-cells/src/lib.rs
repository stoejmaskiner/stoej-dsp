#![no_std]
//! Various smart pointers for very niche (usually RTDSP related) use cases
//!
//! Most of these work from a `no_std` environment as they do not rely on the
//! global allocator.

pub mod bump;
pub mod unsync;
