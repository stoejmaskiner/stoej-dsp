//! Bump-allocated cells

// I don't know what I'm doing, so I'm trying to stay as close as possible
// to these implementations:
// - ref: https://docs.rs/bumpalo/3.15.4/src/bumpalo/boxed.rs.html#147
// - ref: https://doc.rust-lang.org/src/alloc/rc.rs.html#316-319

use core::{
    cell::Cell,
    marker::PhantomData,
    ptr::{self, NonNull},
};

use bumpalo::Bump;

// repr(C) because Rust's std Rc does this. I don't know why, but unsafe scares
// me, so I do it too. Something to do with reordering fields when transmuting.
#[repr(C)]
struct RcBox<T: ?Sized> {
    strong: Cell<usize>,
    weak: Cell<usize>,
    value: T,
}

impl<T: ?Sized> RcBox<T> {
    fn dec_strong(&self) {
        self.strong.set(self.strong.get() - 1);
    }

    fn dec_weak(&self) {
        self.weak.set(self.weak.get() - 1);
    }
}

pub struct Rc<T: ?Sized> {
    ptr: NonNull<RcBox<T>>,
    _phantom: PhantomData<RcBox<T>>,
    // alloc: would be here if we needed to deallocate, but we leak on purpose
}

// public sized
impl<T> Rc<T> {
    pub fn new_in<'alloc>(value: T, a: &'alloc Bump) -> Self {
        let cell = RcBox {
            strong: Cell::new(1),
            weak: Cell::new(1),
            value,
        };
        let ptr = NonNull::<RcBox<T>>::from(a.alloc(cell));
        Self {
            ptr,
            _phantom: PhantomData,
        }
    }
}

// public unsized
impl<T: ?Sized> Rc<T> {
    pub unsafe fn get_mut_unchecked(this: &mut Self) -> &mut T {
        // SAFETY: apparently it's important not to ever create a reference that
        // covers the counters as well, because reasons.
        unsafe { &mut (*this.ptr.as_ptr()).value }
    }
}

// private unsized
impl<T: ?Sized> Rc<T> {
    fn inner(&self) -> &RcBox<T> {
        // SAFETY: inner cell is valid throughout the lifetime of the Rc
        unsafe { self.ptr.as_ref() }
    }
}

impl<T: ?Sized> Drop for Rc<T> {
    fn drop(&mut self) {
        self.inner().dec_strong();
        if self.inner().strong.get() == 0 {
            // SAFETY: uhh... I don't know
            unsafe {
                ptr::drop_in_place(Self::get_mut_unchecked(self));
            }
            // drop the implicit weak reference
            self.inner().dec_weak();
            /*
            if self.inner().weak.get() == 0 {
                // dealloc would happen here, but we're going to leak it on
                // purpose because bump
            }
            */
        }
    }
}
