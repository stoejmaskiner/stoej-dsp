This is a framework for constructing complex audio or DSP effects, by
providing a declarative API to combine simpler processors together into
complex processing graphs, with little run-time overhead.

This crate is heavily optimized, somewhat sacrificing usability. You have
the responsibility of upholding several pre-conditions manually, as Rust's
const generics are used heavily, and they are not mature enough for this
kind of task. Some validation utilities are provided.

This crate also does not provide any audio IO, although integration with the [nih-plug](https://github.com/robbert-vdh/nih-plug) crate is planned.

# Processing Model

Currently, the entire framework is designed for processing [f32] values exclusively. Support for [f64] may be introduced in the future. Support for fixed-point formats is not planned.

Beside several low-level utilities, the bulk of this crate are [processors](processors::api). Processors are explained in detail in the [`processors::api`] module. The basics is that a processor maps a set of inputs to a set of outputs, like a function, but may keep additional state. Processors are linked together to form processor graphs.

An overview of the built-in processors is available in the [`processors`] module. An overview of the "standard library" processors is available in the [`library`] module.

Keep reading for information on some of the others features, unrelated to processors.

# DSL

I'm experimenting with implementing a small DSL for making it easier to write custom processors. No working code is currently available.

# Roadmap

- [ ] core processors
- [ ] processor library
    - [ ] linear filters
    - [ ] interpolated delay
    - [ ] non-linearities
    - [ ] STFT
    - [ ] dynamics
    - [ ] generators
- [ ] event system
    - [ ] MIDI note processors
    - [ ] SysEx processors
    - [ ] MPE note processors
- [ ] NIH-plug integration

<!--
# Processor API

The core of this library is the `Processor` API. A processor is a bit like a function, but it has hidden internal state. Processors can be composed like functions using "processor combinators", which are essentially higher-order processors that combine multiple other processors. The purpose of having hidden state, is that processors can thus extract information that is time-dependent, thus implementing common signal processing algorithms becomes easy.

Processors work on streams of data, so they don't have access to the entire signal, like a traditional DSP library would, they only see a tiny window of the potentially infinite signal, and they have a finite internal state. This allows processors to process signals in real-time. The main use case is for implementing audio processing software, which is optimized for low-dimensional but high sample rate data.

Processors are fairly low-level constructs, so they require some manual tuning to work at their best. To chain two processors, for example, often you will manually have to pass data into the first one, then pass the output of the first to the input of the next in the chain. Some common configurations are already implemented, in the form of processor combinators.

Processors allow us to process vector-like data with the theoretically minimal amount of copies, although this requires quite a bit of labour to set up correctly. Processors can share "scratch space" buffers, and as long as we take care to execute each step in the processor graph in the right order, we can recycle a relatively small amount of scratch space for many processors.

# IIR Filters

- `Biquad`: a generic biquad filter
- `CascadedBiquad`: generic higher order filter using a series of biquads
- `bessel`: flattest pass-band group delay
- `butter`: flattest pass-band response
- `cheby1`: chebychev-1 filter, with ripple in the passband
- `cheby2`: chebychev-2 filter, with ripple in the stop-band
- `crossover`: linkwitz-riley filter, for perfect band splitting
- `elliptic`: with ripple in stop-band and pass-band
- `optimum`: steepest monotonic frequency response
-->

# Licensing

This project is under the GPLv3 license. Dependencies are used in accordance to their respective licenses, including dependencies that we maintain.

# Credits

This framework was inspired by a few DSP languages, namely:
- [Native Instruments Reaktor](https://www.native-instruments.com/en/products/komplete/synths/reaktor-6/)'s "core" node-based visual language.
- [Faust](https://faust.grame.fr/) DSP programming language.
