import numpy as np


def zpk2sos(z, p, k, pairing=None, *, analog=False):
    if pairing is None:
        pairing = "minimal" if analog else "nearest"

    valid_pairings = ["nearest", "keep_odd", "minimal"]
    if pairing not in valid_pairings:
        raise ValueError(
            "pairing must be one of %s, not %s" % (valid_pairings, pairing)
        )

    if analog and pairing != "minimal":
        raise ValueError("for analog zpk2sos conversion, " 'pairing must be "minimal"')

    if len(z) == len(p) == 0:
        if not analog:
            return np.array([[k, 0.0, 0.0, 1.0, 0.0, 0.0]])
        else:
            return np.array([[0.0, 0.0, k, 0.0, 0.0, 1.0]])

    if pairing != "minimal":
        # ensure we have the same number of poles and zeros, and make copies
        p = np.concatenate((p, np.zeros(max(len(z) - len(p), 0))))
        z = np.concatenate((z, np.zeros(max(len(p) - len(z), 0))))
        n_sections = (max(len(p), len(z)) + 1) // 2

        if len(p) % 2 == 1 and pairing == "nearest":
            p = np.concatenate((p, [0.0]))
            z = np.concatenate((z, [0.0]))
        assert len(p) == len(z)
    else:
        if len(p) < len(z):
            raise ValueError(
                "for analog zpk2sos conversion, " "must have len(p)>=len(z)"
            )

        n_sections = (len(p) + 1) // 2

    # Ensure we have complex conjugate pairs
    # (note that _cplxreal only gives us one element of each complex pair):
    z = np.concatenate(_cplxreal(z))
    p = np.concatenate(_cplxreal(p))
    if not np.isreal(k):
        raise ValueError("k must be real")
    k = k.real

    if not analog:
        # digital: "worst" is the closest to the unit circle
        def idx_worst(p):
            print("lmaoo")
            return np.argmin(np.abs(1 - np.abs(p)))

    else:
        # analog: "worst" is the closest to the imaginary axis
        def idx_worst(p):
            print("gneurshk")
            return np.argmin(np.abs(np.real(p)))

    sos = np.zeros((n_sections, 6))

    # Construct the system, reversing order so the "worst" are last
    for si in range(n_sections - 1, -1, -1):
        # Select the next "worst" pole
        p1_idx = idx_worst(p)
        p1 = p[p1_idx]
        p = np.delete(p, p1_idx)

        # Pair that pole with a zero

        if np.isreal(p1) and np.isreal(p).sum() == 0:
            # Special case (1): last remaining real pole
            if pairing != "minimal":
                z1_idx = _nearest_real_complex_idx(z, p1, "real")
                z1 = z[z1_idx]
                z = np.delete(z, z1_idx)
                sos[si] = _single_zpksos([z1, 0], [p1, 0], 1)
            elif len(z) > 0:
                z1_idx = _nearest_real_complex_idx(z, p1, "real")
                z1 = z[z1_idx]
                z = np.delete(z, z1_idx)
                sos[si] = _single_zpksos([z1], [p1], 1)
            else:
                sos[si] = _single_zpksos([], [p1], 1)

        elif (
            len(p) + 1 == len(z)
            and not np.isreal(p1)
            and np.isreal(p).sum() == 1
            and np.isreal(z).sum() == 1
        ):
            # Special case (2): there's one real pole and one real zero
            # left, and an equal number of poles and zeros to pair up.
            # We *must* pair with a complex zero

            z1_idx = _nearest_real_complex_idx(z, p1, "complex")
            z1 = z[z1_idx]
            z = np.delete(z, z1_idx)
            sos[si] = _single_zpksos([z1, z1.conj()], [p1, p1.conj()], 1)

        else:
            if np.isreal(p1):
                prealidx = np.flatnonzero(np.isreal(p))
                p2_idx = prealidx[idx_worst(p[prealidx])]
                p2 = p[p2_idx]
                p = np.delete(p, p2_idx)
            else:
                p2 = p1.conj()

            # find closest zero
            if len(z) > 0:
                z1_idx = _nearest_real_complex_idx(z, p1, "any")
                z1 = z[z1_idx]
                z = np.delete(z, z1_idx)

                if not np.isreal(z1):
                    sos[si] = _single_zpksos([z1, z1.conj()], [p1, p2], 1)
                else:
                    if len(z) > 0:
                        z2_idx = _nearest_real_complex_idx(z, p1, "real")
                        z2 = z[z2_idx]
                        assert np.isreal(z2)
                        z = np.delete(z, z2_idx)
                        sos[si] = _single_zpksos([z1, z2], [p1, p2], 1)
                    else:
                        sos[si] = _single_zpksos([z1], [p1, p2], 1)
            else:
                # no more zeros
                sos[si] = _single_zpksos([], [p1, p2], 1)

    assert len(p) == len(z) == 0  # we've consumed all poles and zeros
    del p, z

    # put gain in first sos
    sos[0][:3] *= k
    return sos


def _cplxreal(z, tol=None):
    """
    Split into complex and real parts, combining conjugate pairs.

    The 1-D input vector `z` is split up into its complex (`zc`) and real (`zr`)
    elements. Every complex element must be part of a complex-conjugate pair,
    which are combined into a single number (with positive imaginary part) in
    the output. Two complex numbers are considered a conjugate pair if their
    real and imaginary parts differ in magnitude by less than ``tol * abs(z)``.

    Parameters
    ----------
    z : array_like
        Vector of complex numbers to be sorted and split
    tol : float, optional
        Relative tolerance for testing realness and conjugate equality.
        Default is ``100 * spacing(1)`` of `z`'s data type (i.e., 2e-14 for
        float64)

    Returns
    -------
    zc : ndarray
        Complex elements of `z`, with each pair represented by a single value
        having positive imaginary part, sorted first by real part, and then
        by magnitude of imaginary part. The pairs are averaged when combined
        to reduce error.
    zr : ndarray
        Real elements of `z` (those having imaginary part less than
        `tol` times their magnitude), sorted by value.

    Raises
    ------
    ValueError
        If there are any complex numbers in `z` for which a conjugate
        cannot be found.

    See Also
    --------
    _cplxpair

    Examples
    --------
    >>> a = [4, 3, 1, 2-2j, 2+2j, 2-1j, 2+1j, 2-1j, 2+1j, 1+1j, 1-1j]
    >>> zc, zr = _cplxreal(a)
    >>> print(zc)
    [ 1.+1.j  2.+1.j  2.+1.j  2.+2.j]
    >>> print(zr)
    [ 1.  3.  4.]
    """

    z = np.atleast_1d(z)
    if z.size == 0:
        return z, z
    elif z.ndim != 1:
        raise ValueError("_cplxreal only accepts 1-D input")

    if tol is None:
        # Get tolerance from dtype of input
        tol = 100 * np.finfo((1.0 * z).dtype).eps

    # Sort by real part, magnitude of imaginary part (speed up further sorting)
    z = z[np.lexsort((abs(z.imag), z.real))]

    # Split reals from conjugate pairs
    real_indices = abs(z.imag) <= tol * abs(z)
    zr = z[real_indices].real

    if len(zr) == len(z):
        # Input is entirely real
        return np.array([]), zr

    # Split positive and negative halves of conjugates
    z = z[~real_indices]
    zp = z[z.imag > 0]
    zn = z[z.imag < 0]

    if len(zp) != len(zn):
        raise ValueError("Array contains complex value with no matching " "conjugate.")

    # Find runs of (approximately) the same real part
    same_real = np.diff(zp.real) <= tol * abs(zp[:-1])
    diffs = np.diff(np.concatenate(([0], same_real, [0])))
    run_starts = np.nonzero(diffs > 0)[0]
    run_stops = np.nonzero(diffs < 0)[0]

    # Sort each run by their imaginary parts
    for i in range(len(run_starts)):
        start = run_starts[i]
        stop = run_stops[i] + 1
        for chunk in (zp[start:stop], zn[start:stop]):
            chunk[...] = chunk[np.lexsort([abs(chunk.imag)])]

    # Check that negatives match positives
    if any(abs(zp - zn.conj()) > tol * abs(zn)):
        raise ValueError("Array contains complex value with no matching " "conjugate.")

    # Average out numerical inaccuracy in real vs imag parts of pairs
    zc = (zp + zn.conj()) / 2

    return zc, zr


def _nearest_real_complex_idx(fro, to, which):
    """Get the next closest real or complex element based on distance"""
    assert which in ("real", "complex", "any")
    order = np.argsort(np.abs(fro - to))
    if which == "any":
        return order[0]
    else:
        mask = np.isreal(fro[order])
        if which == "complex":
            mask = ~mask
        return order[np.nonzero(mask)[0][0]]


def _single_zpksos(z, p, k):
    """Create one second-order section from up to two zeros and poles"""
    sos = np.zeros(6)
    b, a = zpk2tf(z, p, k)
    sos[3 - len(b) : 3] = b
    sos[6 - len(a) : 6] = a
    return sos


def zpk2tf(z, p, k):
    """
    Return polynomial transfer function representation from zeros and poles

    Parameters
    ----------
    z : array_like
        Zeros of the transfer function.
    p : array_like
        Poles of the transfer function.
    k : float
        System gain.

    Returns
    -------
    b : ndarray
        Numerator polynomial coefficients.
    a : ndarray
        Denominator polynomial coefficients.

    """
    z = np.atleast_1d(z)
    k = np.atleast_1d(k)
    if len(z.shape) > 1:
        temp = np.poly(z[0])
        b = np.empty((z.shape[0], z.shape[1] + 1), temp.dtype.char)
        if len(k) == 1:
            k = [k[0]] * z.shape[0]
        for i in range(z.shape[0]):
            b[i] = k[i] * np.poly(z[i])
    else:
        b = k * np.poly(z)
    a = np.atleast_1d(np.poly(p))

    # Use real output if possible. Copied from numpy.poly, since
    # we can't depend on a specific version of numpy.
    if issubclass(b.dtype.type, np.complexfloating):
        # if complex roots are all complex conjugates, the roots are real.
        roots = np.asarray(z, complex)
        pos_roots = np.compress(roots.imag > 0, roots)
        neg_roots = np.conjugate(np.compress(roots.imag < 0, roots))
        if len(pos_roots) == len(neg_roots):
            if np.all(np.sort_complex(neg_roots) == np.sort_complex(pos_roots)):
                b = b.real.copy()

    if issubclass(a.dtype.type, np.complexfloating):
        # if complex roots are all complex conjugates, the roots are real.
        roots = np.asarray(p, complex)
        pos_roots = np.compress(roots.imag > 0, roots)
        neg_roots = np.conjugate(np.compress(roots.imag < 0, roots))
        if len(pos_roots) == len(neg_roots):
            if np.all(np.sort_complex(neg_roots) == np.sort_complex(pos_roots)):
                a = a.real.copy()

    return b, a


print(zpk2sos([1 + 1j, 1 - 1j, 3], [3, 4, 5], 5, analog=False, pairing="nearest"))
