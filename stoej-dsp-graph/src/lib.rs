#![feature(portable_simd)]
#![warn(clippy::pedantic)]

use self::macros::bulk_impl;

pub mod api;
pub mod combinators;
pub mod frame;
pub mod graph;
mod macros;
pub mod mem;
pub mod stream;

#[cfg(feature = "nih_plug")]
pub mod nih_plug_shims;

/// Marker trait for Rust's base numeric types.
trait STDNum: Default {}

bulk_impl!(
    [STDNum],
    [u8, u16, u32, u64, u128, i8, i16, i32, i64, i128, f32, f64, usize, isize]
);

/// The number of elements in a value.
///
/// This is usually `len()` or similar for collections, and `1` for scalars.
pub trait Arity {
    fn arity(&self) -> usize;

    #[inline]
    fn is_nullary(&self) -> bool {
        self.arity() == 0
    }

    #[inline]
    fn is_unary(&self) -> bool {
        self.arity() == 1
    }
}

// ----- impls for foreign types -----

impl<T: STDNum> Arity for T {
    #[inline]
    fn arity(&self) -> usize {
        1
    }
}

#[cfg(test)]
mod integration_tests {
    use crate::{
        combinators::{debug::DebugOps, ProcessorOps},
        mem::Bus,
    };

    #[cfg(test)]
    mod test {
        use crate::frame::*;
        use crate::stream::*;

        #[test]
        fn loop_frame_unframe() {
            let one_one_two = frame![1].dup().cat(frame![2]).play_and_hold();
            let mut stream = one_one_two.frame().unframe();
            assert_eq!(stream.next(), Some(1));
            assert_eq!(stream.next(), Some(1));
            for _ in 0..100 {
                assert_eq!(stream.next(), Some(2));
            }
        }
    }

    #[test]
    fn fibo() {
        let fbk = Bus::new_old();
        fbk.set(1.0);
        let _fibo = fbk
            .read()
            .unit_delay()
            .add(fbk.read())
            .write(fbk)
            .through()
            .println("%");
    }
}
