/// Forward a macro from inner type to outer type.
///
/// This is basically a less powerful derive macro.
macro_rules! forward {
    {$( Self :: $method:ident ( self $( , $arg:ident : $ty:ty )* ) -> $ret:ty ; )*} => {
        $(
            #[inline]
            fn $method(self, $( , $arg : $ty )* ) -> $ret {
                Self::$method(self $(, $ag )* )
            }
        )*
    };
}

/// Implement a lot of traits for a lot of types in a single line. As long as
/// the impl block is empty, for instance in trait aliases.
///
/// For instance `bulk_impl!([A, B], [X, Y])` expands to:
/// ```ignore
/// impl A for X {}
/// impl A for Y {}
/// impl B for X {}
/// impl B for Y {}
/// ```
macro_rules! bulk_impl {
    ($trait:path, $target:path) => {
        impl $trait for $target {}
    };

    ([$trait:path], [$target:path]) => {
        impl $trait for $target {}
    };

    ([$trait:path, $($traits:ty),+], [$target:path]) => {
        bulk_impl!([$trait], [$target]);
        bulk_impl!([$($traits),+], [$target]);
    };

    ([$($traits:ty),+], [$target:path, $($targets:ty),+]) => {
        bulk_impl!([$($traits),+], [$target]);
        bulk_impl!([$($traits),+], [$($targets),+]);
    };
}
pub(crate) use bulk_impl;
