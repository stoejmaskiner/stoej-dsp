use array_macro::array;

use super::Stream;

use std::iter;

/// Alias for [`std::iter::repeat()`].
pub fn constant<T: Clone>(val: T) -> impl Stream<Item = T> {
    iter::repeat(val)
}

/// Holds a single frame, letting you swap it out at runtime.
///
/// This is the building block to turn streams into graphs.
///
/// # Examples
///
/// ```
/// # use stoej_dsp_graph::stream::*;
/// # use stoej_dsp_graph::frame::*;
/// # use stoej_dsp::graph::sources::*;
/// let mut source = FrameBuffer::new();
///
/// ```
pub struct FrameBuffer<const N: usize, T: Clone + Default> {
    current_frame: [T; N],
}

impl<const N: usize, T: Clone + Default> Default for FrameBuffer<N, T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<const N: usize, T: Clone + Default> FrameBuffer<N, T> {
    pub fn new() -> Self {
        Self {
            current_frame: array![T::default(); N],
        }
    }

    pub fn refill(&mut self, from: impl IntoIterator<IntoIter = impl ExactSizeIterator<Item = T>>) {
        let from = from.into_iter();
        debug_assert_eq!(
            from.len(),
            N,
            "can only fill from collection with matching size"
        );
        for (s, f) in self.current_frame.iter_mut().zip(from) {
            *s = f;
        }
    }
}

impl<const N: usize, T: Clone + Default> Iterator for FrameBuffer<N, T> {
    type Item = std::array::IntoIter<T, N>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.current_frame.clone().into_iter())
    }
}

// SAFETY: see Iterator implementation, it can only return Some
unsafe impl<const N: usize, T: Clone + Default> Stream for FrameBuffer<N, T> {}
