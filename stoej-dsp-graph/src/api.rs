use core::simd::prelude::*;
use std::ops::{Add, Div, Mul, Neg, Sub};

mod sealed {
    pub trait Sealed {}
}

pub trait Data:
    Clone
    + Copy
    + Add<Output = Self>
    + Sub<Output = Self>
    + Mul<Output = Self>
    + Div<Output = Self>
    + Neg<Output = Self>
    + Default
    + std::fmt::Debug
    + CmpOps
    + Constants
    + FloatOps
    + SimdOps
    + 'static
    + sealed::Sealed
{
}

pub trait FloatOps: sealed::Sealed + Sized {
    fn abs(self) -> Self;

    fn exp(self) -> Self;

    fn sqrt(self) -> Self {
        todo!()
    }

    fn sin(self) -> Self {
        todo!()
    }

    fn cos(self) -> Self {
        todo!()
    }

    fn tan(self) -> Self {
        todo!()
    }

    fn tanh(self) -> Self {
        todo!()
    }

    fn clamp(self, _min: Self, _max: Self) -> Self {
        todo!()
    }
}

macro_rules! impl_float_ops {
    ($t:ident, $abs:expr, $exp:expr) => {
        impl FloatOps for $t {
            fn abs(self) -> Self {
                $abs(self)
            }

            fn exp(self) -> Self {
                $exp(self)
            }
        }
    };
}

impl_float_ops!(f32, f32::abs, f32::exp);
impl_float_ops!(f32x2, SimdFloat::abs, |x: f32x2| f32x2::from_array([
    x[0].exp(),
    x[1].exp()
]));
impl_float_ops!(f32x4, SimdFloat::abs, |x: f32x4| f32x4::from_array([
    x[0].exp(),
    x[1].exp(),
    x[2].exp(),
    x[3].exp()
]));
// impl_float_ops!(f32x8, SimdFloat::abs);

pub trait CmpOps: sealed::Sealed {
    type Mask;

    fn lt(self, other: Self) -> Self::Mask;

    fn gt(self, other: Self) -> Self::Mask;

    fn le(self, other: Self) -> Self::Mask;

    fn ge(self, other: Self) -> Self::Mask;

    fn eq(self, other: Self) -> Self::Mask;

    fn ne(self, other: Self) -> Self::Mask;

    fn apply_mask(self, other: Self, mask: Self::Mask) -> Self;
}

impl CmpOps for f32 {
    type Mask = bool;

    fn lt(self, other: Self) -> Self::Mask {
        self < other
    }

    fn gt(self, other: Self) -> Self::Mask {
        self > other
    }

    fn le(self, other: Self) -> Self::Mask {
        self <= other
    }

    fn ge(self, other: Self) -> Self::Mask {
        self >= other
    }

    fn eq(self, other: Self) -> Self::Mask {
        self == other
    }

    fn ne(self, other: Self) -> Self::Mask {
        self != other
    }

    fn apply_mask(self, other: Self, mask: Self::Mask) -> Self {
        if mask {
            self
        } else {
            other
        }
    }
}

impl CmpOps for f32x2 {
    type Mask = <f32x2 as SimdFloat>::Mask;

    fn lt(self, other: Self) -> Self::Mask {
        self.simd_lt(other)
    }

    fn gt(self, other: Self) -> Self::Mask {
        self.simd_gt(other)
    }

    fn le(self, other: Self) -> Self::Mask {
        self.simd_le(other)
    }

    fn ge(self, other: Self) -> Self::Mask {
        self.simd_ge(other)
    }

    fn eq(self, other: Self) -> Self::Mask {
        self.simd_eq(other)
    }

    fn ne(self, other: Self) -> Self::Mask {
        self.simd_ne(other)
    }

    fn apply_mask(self, other: Self, mask: Self::Mask) -> Self {
        mask.select(self, other)
    }
}

impl CmpOps for f32x4 {
    type Mask = <f32x4 as SimdFloat>::Mask;

    fn lt(self, other: Self) -> Self::Mask {
        self.simd_lt(other)
    }

    fn gt(self, other: Self) -> Self::Mask {
        self.simd_gt(other)
    }

    fn le(self, other: Self) -> Self::Mask {
        self.simd_le(other)
    }

    fn ge(self, other: Self) -> Self::Mask {
        self.simd_ge(other)
    }

    fn eq(self, other: Self) -> Self::Mask {
        self.simd_eq(other)
    }

    fn ne(self, other: Self) -> Self::Mask {
        self.simd_ne(other)
    }

    fn apply_mask(self, other: Self, mask: Self::Mask) -> Self {
        mask.select(self, other)
    }
}

macro_rules! impl_data {
    ($t:ident) => {
        impl crate::api::sealed::Sealed for $t {}
        impl crate::api::Data for $t {}
    };
}

impl_data!(f32);
impl_data!(f32x2);
impl_data!(f32x4);
//impl_data!(f32x8);

/// Named constants for use in generic programming
pub trait Constants: Sized {
    const ZERO: Self;
    const ONE: Self;
    const TWO: Self;
    const EPSILON: Self;
}

impl Constants for f32 {
    const ZERO: Self = 0.0;

    const ONE: Self = 1.0;

    const TWO: Self = 2.0;

    const EPSILON: Self = f32::EPSILON;
}

impl Constants for f32x2 {
    const ZERO: Self = f32x2::from_array([0.0; 2]);

    const ONE: Self = f32x2::from_array([1.0; 2]);

    const TWO: Self = f32x2::from_array([2.0; 2]);

    const EPSILON: Self = f32x2::from_array([f32::EPSILON; 2]);
}

impl Constants for f32x4 {
    const ZERO: Self = f32x4::from_array([0.0; 4]);

    const ONE: Self = f32x4::from_array([1.0; 4]);

    const TWO: Self = f32x4::from_array([2.0; 4]);

    const EPSILON: Self = f32x4::from_array([f32::EPSILON; 4]);
}

pub trait Processor<F: Data>: Iterator<Item = F> {}

// blanket impl for std iterators
impl<F: Data, T: Iterator<Item = F>> Processor<F> for T {}

pub trait Sink: Iterator<Item = ()> {}

impl<T: Iterator<Item = ()>> Sink for T {}

pub trait SimdOps {
    fn splat(x: f32) -> Self;
}

impl SimdOps for f32 {
    fn splat(x: f32) -> Self {
        x
    }
}

impl SimdOps for f32x2 {
    fn splat(x: f32) -> Self {
        f32x2::splat(x)
    }
}

impl SimdOps for f32x4 {
    fn splat(x: f32) -> Self {
        f32x4::splat(x)
    }
}
