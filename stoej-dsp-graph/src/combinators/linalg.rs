//! Linear algebra primitives for SIMD processors.

pub trait LinalgOps {
    /// Dot product of vectors
    fn inner(self, other: Self) -> Self;
}

pub trait LinalgOps2 {
    /// Outer product of 2D vectors into a 2x2 matrix
    fn outer(self, other: Self);
}

pub trait LinalgOps4 {
    /// transpose 2x2 matrix
    fn transpose(self) -> Self;
}
