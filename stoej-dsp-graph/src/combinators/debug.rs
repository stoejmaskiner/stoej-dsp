use itertools::Itertools;

use crate::api::{Data, Processor, Sink};

pub trait DebugOps<F: Data>: Processor<F> + Sized {
    /// # Syntax
    /// The character `%` is replaced with the value yielded by the processor.
    /// To print a literal `%`, it should be escaped like so: `%%`.
    ///
    /// # Examples
    /// ```rust
    /// # use stoej_dsp_graph::combinators::constant;
    /// # use stoej_dsp_graph::combinators::debug::DebugOps;
    /// assert_eq!(constant(1.0).format("escaped: %%, unescaped: %").next().expect(""), "escaped: %, unescaped: 1.0");
    /// ```
    fn format(self, fmt: &'static str) -> impl Iterator<Item = String> {
        let parts = fmt.split(r"%%");
        self.map(move |x| {
            let number_fmt = format!("{:?}", x);
            Itertools::intersperse(
                parts.clone().map(|part| part.replace('%', &number_fmt)),
                "%".to_owned(),
            )
            .collect::<String>()
        })
    }

    /// See [`format`] for a description of the syntax
    fn print(self, fmt: &'static str) -> impl Sink {
        self.format(fmt).map(|s| print!("{}", s))
    }

    /// See [`format`] for a description of the syntax
    fn println(self, fmt: &'static str) -> impl Sink {
        self.format(fmt).map(|s| println!("{}", s))
    }
}

impl<F: Data, P: Processor<F>> DebugOps<F> for P {}
