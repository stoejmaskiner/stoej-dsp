use crate::api::{Data, Processor};

use super::{constant, filter::OnePole, BusRead, ProcessorOps};

pub fn rise_follower<F: Data>(upstream: impl Processor<F>, sr: BusRead<f32>) -> impl Processor<F> {
    let (u1, u2) = upstream.split();
    let is_rising = ProcessorOps::ge(u1.diff(), constant(F::ZERO));
    u2.one_pole(
        constant(F::TWO).apply_mask(constant(F::EPSILON), is_rising),
        sr,
    )
}
