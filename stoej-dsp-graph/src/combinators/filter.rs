use crate::api::{Data, Processor};

use super::{Bus, BusRead, ProcessorOps, SimdOps1};

// pub struct OnePole<F: Data, TAU: Processor<F>> {
//     y_z1: F,
//     tau: TAU,
//     sr: BusRead<F>,
// }

// impl<F: Data, TAU: Processor<F>> Iterator for OnePole<F, TAU> {
//     type Item = F;

//     fn next(&mut self) -> Option<Self::Item> {
//         let t =
//         let a = self.tau.next()?
//     }
// }

/// A one-pole IIR lowpass filter
pub trait OnePole<F: Data>: Processor<F> + Sized {
    fn one_pole(self, tau: impl Processor<F>, sr: BusRead<f32>) -> impl Processor<F> {
        let a = sr.splat().mul(tau).recip().neg().exp();
        let y_z1 = Bus::new_old();
        a.lerp(self, y_z1.read()).write(y_z1).through()
    }
}

impl<F: Data, P: Processor<F>> OnePole<F> for P {}
