use std::iter;

use crate::frame::{frame, Frame};

pub mod sources;

/// An infinite iterator
///
/// # Safety
/// Iterator must never yield `None`, if it needs to stop yielding items it
/// can only do so by panicking or some external signaling system.
pub unsafe trait Stream: Iterator {
    fn step(&mut self) -> Self::Item {
        let maybe = self.next();
        debug_assert!(
            maybe.is_some(),
            "safety violation: undefined behavior in Release builds"
        );
        unsafe { maybe.unwrap_unchecked() }
    }
}

// ----- impls for foreign types -----

// SAFETY: map only operates on the inner value, so the "ininiteness" is not affected
unsafe impl<T, I: Stream, F: FnMut(I::Item) -> T> Stream for iter::Map<I, F> {}

// SAFETY: std's docs say repeat is endless
unsafe impl<T: Clone> Stream for iter::Repeat<T> {}

// ===== STREAM OPS =====

pub trait StreamOps: Stream + Sized
where
    Self::Item: Clone,
{
    fn frame(self) -> impl Stream<Item = impl Frame<Item = Self::Item>> {
        self.map(|x| frame![x])
    }
}

// ----- blanket impls -----

impl<T: Stream> StreamOps for T where T::Item: Clone {}

pub trait FrameStreamOps<F: Frame>: Stream<Item = F> + Sized {
    /// Unwrap a stream of frames of size 1, into a stream of their elements
    ///
    /// # Examples
    ///
    /// ```
    /// # use stoej_dsp_graph::stream::*;
    /// # use stoej_dsp_graph::frame::*;
    /// use stoej_dsp_graph::stream::sources::constant;
    /// let mut stream = constant(frame![1]).unframe();
    /// assert_eq!(stream.next(), Some(1));
    /// assert_eq!(stream.next(), Some(1));
    /// assert_eq!(stream.next(), Some(1));
    /// ```
    fn unframe(self) -> impl Stream<Item = F::Item> {
        self.map(|mut x| {
            assert_eq!(x.len(), 1, "can only unframe frame of size 1");
            // SAFETY: just checked here ^^^
            unsafe { x.next().unwrap_unchecked() }
        })
    }
}

impl<F: Frame, T: Stream<Item = F>> FrameStreamOps<F> for T {}
