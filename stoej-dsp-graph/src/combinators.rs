use std::{
    cell::{Cell, RefCell},
    iter::{once, repeat},
    rc::Rc,
    simd::{f32x2, f32x4, num::SimdFloat},
};

use enum_as_inner::EnumAsInner;

use crate::{
    api::{Data, FloatOps, Processor},
    mem::{Bus, BusRead, BusWrite, BusWriteThrough},
};

pub mod debug;
pub mod envelope;
pub mod filter;
pub mod linalg;
pub mod stats;

// separate to avoid object safety issues
pub trait ProcessorOps<F: Data>: Processor<F> + Sized {
    fn add(self, other: impl Processor<F>) -> impl Processor<F> {
        self.zip(other).map(|(a, b)| a + b)
    }

    fn sub(self, other: impl Processor<F>) -> impl Processor<F> {
        self.zip(other).map(|(a, b)| a - b)
    }

    fn mul(self, other: impl Processor<F>) -> impl Processor<F> {
        self.zip(other).map(|(a, b)| a * b)
    }

    fn div(self, other: impl Processor<F>) -> impl Processor<F> {
        self.zip(other).map(|(a, b)| a / b)
    }

    fn neg(self) -> impl Processor<F> {
        self.map(|x| -x)
    }

    fn complement(self) -> impl Processor<F> {
        constant(F::ONE).sub(self)
    }

    fn recip(self) -> impl Processor<F> {
        constant(F::ONE).div(self)
    }

    fn abs(self) -> impl Processor<F> {
        self.map(|x| x.abs())
    }

    fn exp(self) -> impl Processor<F> {
        self.map(|x| x.exp())
    }

    fn lt(self, other: impl Processor<F>) -> impl Iterator<Item = F::Mask> {
        self.zip(other).map(|(a, b)| F::lt(a, b))
    }

    fn gt(self, other: impl Processor<F>) -> impl Iterator<Item = F::Mask> {
        self.zip(other).map(|(a, b)| F::gt(a, b))
    }

    fn le(self, other: impl Processor<F>) -> impl Iterator<Item = F::Mask> {
        self.zip(other).map(|(a, b)| F::le(a, b))
    }

    fn ge(self, other: impl Processor<F>) -> impl Iterator<Item = F::Mask> {
        self.zip(other).map(|(a, b)| F::ge(a, b))
    }

    fn eq(self, other: impl Processor<F>) -> impl Iterator<Item = F::Mask> {
        self.zip(other).map(|(a, b)| F::eq(a, b))
    }

    fn ne(self, other: impl Processor<F>) -> impl Iterator<Item = F::Mask> {
        self.zip(other).map(|(a, b)| F::ne(a, b))
    }

    fn apply_mask(
        self,
        other: impl Processor<F>,
        mask: impl Iterator<Item = F::Mask>,
    ) -> impl Processor<F> {
        self.zip(other)
            .zip(mask)
            .map(|((a, b), m)| a.apply_mask(b, m))
    }

    /// Lazily split a processor into two linked processors, yielding the same
    /// values.
    ///
    /// This does not clone any of the upstream iterators, instead it uses
    /// a shared variable to share the output of the upstream iterator between
    /// the two branches. This is somewhat inefficient, but more ergonomic.
    ///
    /// For faster performance, consider using [`Bus`]es directly. This requires
    /// some more care, as buses are generally more error prone.
    fn split(self) -> (impl Processor<F>, impl Processor<F>) {
        Split::new_pair(self)
    }

    fn unit_delay(self) -> impl Processor<F> {
        once(F::default()).chain(self)
    }

    /// Finite difference
    fn diff(self) -> impl Processor<F> {
        let (a, b) = self.split();
        a.sub(b.unit_delay())
    }

    /// Finite sum
    fn integrate(self) -> impl Processor<F> {
        let fbk = Bus::new_old();
        self.add(fbk.read()).write(fbk).through()
    }

    /// Write to a [`Bus`].
    fn write(self, var: Bus<F>) -> BusWrite<F, Self> {
        var.accept_upstream(self)
    }

    /// Linear interpolation between two processors, using this processor as the
    /// interpolation parameter.
    fn lerp(self, lhs: impl Processor<F>, rhs: impl Processor<F>) -> impl Processor<F> {
        let (u, v) = self.split();
        lhs.mul(u.complement()).add(rhs.mul(v))
    }
}

#[derive(EnumAsInner)]
pub enum Split<F: Data, P: Processor<F>> {
    Unknown {
        bus: Rc<Bus<F>>,
        upstream: Rc<Cell<Option<P>>>,
    },
    Original(BusWriteThrough<F, P>),
    Clone(BusRead<F>),
}

impl<F: Data, P: Processor<F>> Split<F, P> {
    fn new_pair(upstream: P) -> (Self, Self) {
        let bus = Rc::new(Bus::new_old());
        let upstream = Rc::new(Cell::new(Some(upstream)));
        let split_a = Self::Unknown {
            bus: bus.clone(),
            upstream: upstream.clone(),
        };
        let split_b = Self::Unknown { bus, upstream };
        assert_eq!(
            Rc::strong_count(split_a.as_unknown().expect("infallible").0),
            2,
            "post-condition violation: splitting a processor should produce exactly 2 linked `Rc<Bus>`. Their refcount is used for signaling purposes, so it should be exactly 2."
        );
        (split_a, split_b)
    }
}

impl<F: Data, P: Processor<F>> Iterator for Split<F, P> {
    type Item = F;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            Split::Unknown { bus, upstream } => {
                // if both sides are still in the Unknown state, this becomes
                // the original copy.
                if Rc::strong_count(bus) == 2 {
                    *self = Split::Original(
                        bus.as_ref()
                            .clone()
                            .accept_upstream(upstream.replace(None).expect(
                                "upstream should not be None while split is being initialized",
                            ))
                            .through(),
                    );
                    // TODO: tail recursion?
                    return self.next();
                }
                // if we got here, this split is evaluated after its sibling, so
                // it should not advance the upstream, so we change its state to clone
                *self = Split::Clone(bus.as_ref().clone().read());
                // TODO: tail recursion?
                self.next()
            }
            Split::Original(writer) => writer.next(),
            Split::Clone(reader) => reader.next(),
        }
    }
}

pub trait SimdOps<F: Data + SimdFloat<Scalar = f32>>: Processor<F> + Sized {
    fn packed_sum(self) -> impl Processor<f32> {
        self.map(|x| x.reduce_sum())
    }

    fn packed_max(self) -> impl Processor<f32> {
        self.map(|x| x.reduce_max())
    }

    fn packed_min(self) -> impl Processor<f32> {
        self.map(|x| x.reduce_min())
    }

    fn packed_prod(self) -> impl Processor<f32> {
        self.map(|x| x.reduce_product())
    }
}

pub trait SimdOps1: Processor<f32> + Sized {
    fn chunk_2(self) -> impl Processor<f32x2> {
        Chunk2(self)
    }

    fn chunk_4(self) -> impl Processor<f32x4> {
        Chunk4(self)
    }

    fn splat<G: crate::api::SimdOps + Data>(self) -> impl Processor<G> {
        self.map(|x| G::splat(x))
    }
}

impl<P: Processor<f32>> SimdOps1 for P {}

pub struct Chunk2<P: Processor<f32>>(P);

impl<P: Processor<f32>> Iterator for Chunk2<P> {
    type Item = f32x2;

    fn next(&mut self) -> Option<Self::Item> {
        Some(f32x2::from_array([self.0.next()?, self.0.next()?]))
    }
}

pub struct Chunk4<P: Processor<f32>>(P);

impl<P: Processor<f32>> Iterator for Chunk4<P> {
    type Item = f32x4;

    fn next(&mut self) -> Option<Self::Item> {
        Some(f32x4::from_array([
            self.0.next()?,
            self.0.next()?,
            self.0.next()?,
            self.0.next()?,
        ]))
    }
}

pub trait SimdOps2: Processor<f32x2> + Sized {
    fn unpack_2(self) -> (impl Processor<f32>, impl Processor<f32>) {
        let (a, b) = self.split();
        (a.map(|x| x.to_array()[0]), b.map(|x| x.to_array()[1]))
    }

    fn chunk_2x2(self) -> impl Processor<f32x4> {
        Chunk2x2(self)
    }

    fn unchunk_2(self) -> impl Processor<f32> {
        self.flat_map(|x| *x.as_array())
    }
}

pub struct Chunk2x2<P: Processor<f32x2>>(P);

impl<P: Processor<f32x2>> Iterator for Chunk2x2<P> {
    type Item = f32x4;

    fn next(&mut self) -> Option<Self::Item> {
        let a = self.0.next()?;
        let b = self.0.next()?;
        Some(f32x4::from_array([
            a.as_array()[0],
            a.as_array()[1],
            b.as_array()[0],
            b.as_array()[1],
        ]))
    }
}

impl<P: Processor<f32x2>> SimdOps2 for P {}

pub trait SimdOps4: Processor<f32x4> + Sized {
    fn unpack_4(
        self,
    ) -> (
        impl Processor<f32>,
        impl Processor<f32>,
        impl Processor<f32>,
        impl Processor<f32>,
    ) {
        let (ab, cd) = self.split();
        let (a, b) = ab.split();
        let (c, d) = cd.split();
        (
            a.map(|x| x.to_array()[0]),
            b.map(|x| x.to_array()[1]),
            c.map(|x| x.to_array()[2]),
            d.map(|x| x.to_array()[3]),
        )
    }

    fn unpack_2x2(self) -> (impl Processor<f32x2>, impl Processor<f32x2>) {
        let (a, b) = self.split();
        (
            a.map(|x| f32x2::from_slice(&x.to_array()[0..2])),
            b.map(|x| f32x2::from_slice(&x.to_array()[2..4])),
        )
    }

    fn unchunk_4(self) -> impl Processor<f32> {
        self.flat_map(|x| *x.as_array())
    }

    fn unchunk_2x2(self) -> impl Processor<f32x2> {
        self.flat_map(|x| {
            [
                f32x2::from_slice(&x.as_array()[0..2]),
                f32x2::from_slice(&x.as_array()[2..4]),
            ]
        })
    }
}

// pub struct Unchunk4<P: Processor<f32x4>>(P);

// impl<P: Processor<f32x4>> Iterator for Unchunk4<P> {
//     type Item = f32;

//     fn next(&mut self) -> Option<Self::Item> {}
// }

impl<P: Processor<f32x4>> SimdOps4 for P {}

impl<F: Data, T: Processor<F>> ProcessorOps<F> for T {}
impl<F: Data + SimdFloat<Scalar = f32>, T: Processor<F>> SimdOps<F> for T {}

#[inline(always)]
pub fn constant<F: Data>(x: F) -> impl Processor<F> {
    repeat(x)
}

pub fn forward_declare<F: Data>() -> (ForwardDeclared<F>, ForwardDeclaredInitializer<F>) {
    let cell = Rc::new(RefCell::new(None));
    (
        ForwardDeclared(cell.clone()),
        ForwardDeclaredInitializer(cell.clone()),
    )
}

pub struct ForwardDeclared<F: Data>(Rc<RefCell<Option<Box<dyn Processor<F>>>>>);

impl<F: Data> Iterator for ForwardDeclared<F> {
    type Item = F;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(ref mut inner) = *self.0.borrow_mut() {
            return inner.next();
        }
        panic!("uninitialized forward declared processor");
    }
}

pub struct ForwardDeclaredInitializer<F: Data>(Rc<RefCell<Option<Box<dyn Processor<F>>>>>);

impl<F: Data> ForwardDeclaredInitializer<F> {
    pub fn init(self, inner: impl Processor<F> + 'static) {
        *self.0.borrow_mut() = Some(Box::new(inner));
    }
}

// impl<F: Data, P: Processor<F>> ForwardDeclare<F, P> {
//     fn init(self: Rc<Self>) -> {
//     }
// }

pub fn pack_4(
    ins: (
        impl Processor<f32>,
        impl Processor<f32>,
        impl Processor<f32>,
        impl Processor<f32>,
    ),
) -> impl Processor<f32x4> {
    ins.0
        .zip(ins.1)
        .zip(ins.2)
        .zip(ins.3)
        .map(|(((a, b), c), d)| f32x4::from_array([a, b, c, d]))
}

pub fn pack_2(ins: (impl Processor<f32>, impl Processor<f32>)) -> impl Processor<f32x2> {
    ins.0.zip(ins.1).map(|(a, b)| f32x2::from_array([a, b]))
}

pub fn pack_2x2(ins: (impl Processor<f32x2>, impl Processor<f32x2>)) -> impl Processor<f32x4> {
    let (a, b) = ins.0.unpack_2();
    let (c, d) = ins.1.unpack_2();
    pack_4((a, b, c, d))
}

pub fn counter() -> impl Processor<f32> {
    constant(1.0).integrate().unit_delay()
}

#[cfg(test)]
mod test {
    use std::simd::f32x4;

    use super::{constant, counter, forward_declare, Bus, ProcessorOps, SimdOps};

    #[test]
    fn add() {
        let foo = constant(2.0);
        let bar = constant(3.0);
        assert_eq!(foo.add(bar).next(), Some(5.0));
    }

    #[test]
    fn packed_sum() {
        let foo = constant(f32x4::from_array([1.0, 2.0, 3.0, 4.0]));
        assert_eq!(foo.packed_sum().next(), Some(10.0));
    }

    #[test]
    fn split() {
        let foo = counter();
        let (mut foo, mut bar) = foo.split();
        for _ in 0..10 {
            assert_eq!(foo.next(), bar.next());
        }
    }

    #[test]
    fn split_reverse() {
        // this covers an edge case that was previously a bug
        // the order in which .next() is called on the two splits should not
        // affect the result.
        let foo = counter();
        let (mut bar, mut foo) = foo.split();
        for _ in 0..10 {
            assert_eq!(foo.next(), bar.next());
        }
    }

    #[test]
    fn unit_delay() {
        let foo = constant(1.0);
        let mut bar = foo.unit_delay();
        assert_eq!(bar.next(), Some(f32::default()));
        assert_eq!(bar.next(), Some(1.0));
    }

    #[test]
    fn diff() {
        let mut foo = constant(1.0).diff();
        assert_eq!(foo.next(), Some(1.0));
        for _ in 0..10 {
            assert_eq!(foo.next(), Some(0.0));
        }
    }

    #[test]
    fn test_forward_declare() {
        let (fd, handle) = forward_declare::<f32>();
        let mut lmao = fd.unit_delay();
        handle.init(constant(1.0));
        assert_eq!(lmao.next(), Some(0.0));
        assert_eq!(lmao.next(), Some(1.0));
    }

    #[test]
    fn integrate() {
        let mut counter = constant(1.0).integrate();
        for i in 1..=100 {
            assert_eq!(counter.next(), Some(i as f32));
        }
    }

    #[test]
    fn diff_integ() {
        let mut foo = constant(1.0).diff().integrate();
        for _ in 0..10 {
            assert_eq!(foo.next(), Some(1.0));
        }
    }

    #[test]
    fn feedback() {
        let foo = Bus::new_old();
        let mut thingy = foo.read().unit_delay().write(foo).through();
        assert_eq!(thingy.next(), Some(0.0));
        assert_eq!(thingy.next(), Some(0.0));
        assert_eq!(thingy.next(), Some(0.0));
    }

    #[test]
    fn test_counter() {
        for (i, j) in counter().take(10).enumerate() {
            assert_eq!(i, j as usize)
        }
    }

    #[test]
    fn lerp() {
        let foo = constant(f32x4::from_array([1.0, 2.0, 3.0, 4.0]));
        let bar = constant(f32x4::from_array([10.0, 20.0, 30.0, 40.0]));
        let t = constant(f32x4::from_array([0.0, 0.25, 0.5, 1.0]));
        let mut baz = t.lerp(foo, bar);
        assert_eq!(baz.next(), Some(f32x4::from_array([1.0, 6.5, 16.5, 40.0])));
    }
}
