//! Memory primitives for efficient sharing of data in DSP graph

use std::{cell::Cell, marker::PhantomData, rc::Rc};

use stoej_dsp_buffer::ring_buffer::{BufferLen, Queue, SupportedBufferLen};

use crate::{
    api::{Data, Processor},
    frame::Frame,
    stream::Stream,
};

mod sealed {
    pub trait Sealed {}
}

/// Signaling across the DSP graph
pub type Signal = OptionCell<()>;

// TODO: right now this is essentially a newtype around `Rc<Cell<Option<T>>>`
//       that provides better ergonomics. Perhaps a dedicated unsafe implementation
//       could be made that only supports needed features and may be slightly
//       faster.
/// A cell that maybe holds a value. Essentially a shared Option.
pub struct OptionCell<T>(Rc<Cell<Option<T>>>);

impl<T> Clone for OptionCell<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<T> Default for OptionCell<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> OptionCell<T> {
    /// Create a new empty cell
    pub fn new() -> Self {
        Self(Rc::new(Cell::new(None)))
    }

    /// Create a cell from an option
    pub fn from_option(o: Option<T>) -> Self {
        Self(Rc::new(Cell::new(o)))
    }

    /// Create a cell with a value
    pub fn from_value(t: T) -> Self {
        Self(Rc::new(Cell::new(Some(t))))
    }

    pub fn set(&self, val: T) {
        self.0.set(Some(val));
    }

    pub fn unset(&self) {
        self.0.set(None);
    }

    pub fn take(&self) -> Option<T> {
        self.0.take()
    }

    pub fn replace(&self, val: T) -> Option<T> {
        self.0.replace(Some(val))
    }

    pub fn is_set(&self) -> bool {
        let inner = self.0.take();
        let res = inner.is_some();
        self.0.set(inner);
        res
    }

    pub fn apply(&self, f: impl Fn(T) -> T) {
        self.0.set(self.0.take().map(f));
    }

    pub fn swap(&self, other: &Self) {
        self.0.swap(&other.0);
    }
}

impl<T: Clone> OptionCell<T> {
    pub fn get(&self) -> Option<T> {
        let inner = self.0.take();
        let res = inner.clone();
        self.0.set(inner);
        res
    }
}

pub struct UnsyncSender<T>(OptionCell<T>);

impl<T> UnsyncSender<T> {
    /// Try to send a value to the channel
    pub fn send(&self, val: T) -> Result<(), ()> {
        (!self.0.is_set()).then(|| self.0.set(val)).ok_or(())
    }
}

#[derive(Clone)]
pub struct UnsyncReceiver<T>(OptionCell<T>);

impl<T> UnsyncReceiver<T> {
    /// Try to receive a value from the channel
    pub fn receive(&self) -> Result<T, ()> {
        self.0.take().ok_or(())
    }
}

/// A channel, like Rust's [`channel`], but deliberately not thread-safe
pub fn unsync_channel<T>() -> (UnsyncSender<T>, UnsyncReceiver<T>) {
    let cell = OptionCell::new();
    let tx = UnsyncSender(cell.clone());
    let rx = UnsyncReceiver(cell);
    (tx, rx)
}

pub struct UnsyncQueueSender<T: Data, const N: usize>(Rc<Queue<T, N>>, PhantomData<T>)
where
    BufferLen<N>: SupportedBufferLen;

pub struct UnsyncQueueReceiver<T: Data, const N: usize>(Rc<Queue<T, N>>, PhantomData<T>)
where
    BufferLen<N>: SupportedBufferLen;

// pub trait CloningOptionCell<T: Clone>: sealed::Sealed {
//     fn get(&self) -> Option<T>;
// }

// impl<T> sealed::Sealed for OptionCell<T> {}

// impl<T: Clone> CloningOptionCell<T> for OptionCell<T> {
//     fn get(&self) -> Option<T> {
//         let inner = self.0.take();
//         let res = inner.clone();
//         self.0.set(inner);
//         res
//     }
// }

/// A variable with shared access, allows remote parts of the processor graph
/// to communicate with each other directly.
///
/// This is a low-level primitive for graph construction, it is used extensively
/// in other provided combinators. It is generally easier to use one of the
/// other combinators, like [`Split`], as `Bus` is fairly crude, imperative and
/// error prone. That said, you may want to use `Bus` for optimization purposes,
/// as the other primitives have some overhead to avoid unexpected behavior.
#[derive(Clone)]
pub struct Bus<T> {
    mem: OptionCell<T>,
}

// public
impl<F> Bus<F> {
    pub fn new(val: F) -> Self {
        let cell = OptionCell::new();
        cell.set(val);
        Self { mem: cell }
    }

    pub fn read(&self) -> BusRead<F> {
        BusRead {
            mem: self.mem.clone(),
        }
    }

    pub fn set(&self, sr: F) {
        self.mem.set(sr)
    }
}

// public
impl<F: Clone, const N: usize> Bus<[F; N]> {
    pub fn frames(&self) -> impl Stream<Item = impl Frame<Item = F>> {
        self.read().map(|f| f.into_iter())
    }
}

// public
impl<F: Default> Bus<F> {
    #[deprecated]
    pub fn new_old() -> Self {
        Self::new(F::default())
    }
}

// private
impl<F: Data> Bus<F> {
    pub(crate) fn accept_upstream<P: Processor<F>>(self, upstream: P) -> BusWrite<F, P> {
        BusWrite {
            upstream,
            mem: self.mem,
        }
    }
}

pub struct BusWrite<F: Data, P: Processor<F>> {
    upstream: P,
    mem: OptionCell<F>,
}

impl<F: Data, P: Processor<F>> BusWrite<F, P> {
    pub fn through(self) -> BusWriteThrough<F, P> {
        BusWriteThrough {
            upstream: self.upstream,
            mem: self.mem,
        }
    }
}

impl<F: Data, P: Processor<F>> Iterator for BusWrite<F, P> {
    type Item = ();

    fn next(&mut self) -> Option<Self::Item> {
        self.mem.set(self.upstream.next()?);
        Some(())
    }
}

pub struct BusWriteThrough<F: Data, P: Processor<F>> {
    upstream: P,
    mem: OptionCell<F>,
}

impl<F: Data, P: Processor<F>> Iterator for BusWriteThrough<F, P> {
    type Item = F;

    fn next(&mut self) -> Option<Self::Item> {
        let val = self.upstream.next()?;
        self.mem.set(val);
        Some(val)
    }
}

#[derive(Clone)]
pub struct BusRead<F> {
    mem: OptionCell<F>,
}

impl<F: Clone> Iterator for BusRead<F> {
    type Item = F;

    fn next(&mut self) -> Option<Self::Item> {
        assert!(
            self.mem.is_set(),
            "Bus yielded None. BusRead is a Stream, it cannot yield None."
        );
        self.mem.get()
    }
}

// SAFETY: see Iterator impl for BusRead, it has an assertion that prevents
//         returning None
unsafe impl<F: Clone> Stream for BusRead<F> {}

#[cfg(test)]
mod test {
    use super::unsync_channel;

    #[test]
    fn test() {
        let (tx, rx) = unsync_channel::<f32>();
        tx.send(0.0).unwrap();
        assert_eq!(rx.receive().unwrap(), 0.0);
    }
}
