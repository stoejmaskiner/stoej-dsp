use crate::stream::Stream;
use anyhow::{ensure, Result};

use std::ops::Range;

mod sealed {
    pub trait Sealed {}
}

pub trait ExpectExactSize {
    type Output: Frame;
    fn expect_exact_size(self, size: usize) -> Self::Output;
}

impl<I: DoubleEndedIterator + Clone> ExpectExactSize for I {
    type Output = ExpectExactSizeWrapper<I>;

    fn expect_exact_size(self, size: usize) -> Self::Output {
        ExpectExactSizeWrapper {
            upstream: self,
            size,
        }
    }
}

#[doc(hidden)]
#[derive(Clone)]
pub struct ExpectExactSizeWrapper<I: DoubleEndedIterator> {
    upstream: I,
    size: usize,
}

impl<I: DoubleEndedIterator> Iterator for ExpectExactSizeWrapper<I> {
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        match (self.upstream.next(), self.size) {
            (Some(_), 0) => panic!("exact size iterator was longer than expected"),
            (some @ Some(_), _) => {
                self.size -= 1;
                some
            }
            (None, 0) => None,
            (None, _) => panic!("exact size iterator was shorter than expected"),
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.size, Some(self.size))
    }
}

impl<I: DoubleEndedIterator> ExactSizeIterator for ExpectExactSizeWrapper<I> {
    // fn len(&self) -> usize {
    //     let (lower, upper) = self.size_hint();
    //     debug_assert_eq!(upper, Some(lower));
    //     lower
    // }
}

impl<I: DoubleEndedIterator> DoubleEndedIterator for ExpectExactSizeWrapper<I> {
    fn next_back(&mut self) -> Option<Self::Item> {
        match (self.upstream.next_back(), self.size) {
            (Some(_), 0) => panic!("exact size iterator was longer than expected"),
            (some @ Some(_), _) => {
                self.size -= 1;
                some
            }
            (None, 0) => None,
            (None, _) => panic!("exact size iterator was shorter than expected"),
        }
    }
}

//impl<I: DoubleEndedIterator> Frame for IntoFrameWrapper<I> {}

// ===== FRAME =====

#[macro_export]
#[doc(hidden)]
/// Conveniently define a constant frame with a similar syntax to the [`vec!`] macro
///
/// # Examples
/// ```
/// # use stoej_dsp_graph::frame;
/// let mut frame = frame![1, 2, 3];
/// assert_eq!(frame.next(), Some(1));
/// assert_eq!(frame.next(), Some(2));
/// assert_eq!(frame.next(), Some(3));
/// assert_eq!(frame.next(), None);
/// ```
macro_rules! frame {
    [$($t:tt),*] => {
        [$($t),*].into_iter()
    };
    [$a:tt; $b:tt] => {
        array![$a; $b].into_iter()
    }
}
pub use frame;

/// An iterator with an exact size, used as a generic collection.
///
/// Frames get cloned a lot, so they should be cheap to clone. They should
/// be small and should not allocate or deallocate when cloned. Slices may be
/// used, but pose challenges in terms of managing lifetimes. You can use
/// [`std::rc::Rc`] or a statically allocated arena to avoid lifetimes.
///
/// Some examples of cheap frames:
/// - Small arrays (< 64 bytes), e.g. `[f32; 16]`
/// - Reference counted slices, e.g. `Rc<[f32]>`
/// - Statically allocated slices, e.g. `&'static [f32]`
pub trait Frame: ExactSizeIterator + DoubleEndedIterator + Clone {
    /// Create a new frame with the same arity, filled with default value.
    ///
    /// # Examples
    /// ```
    /// # use stoej_dsp_graph::frame::*;
    /// let template = frame![1, 2, 3];
    /// let mut frame = template.defaults_like();
    /// assert_eq!(frame.next(), Some(0));
    /// assert_eq!(frame.next(), Some(0));
    /// assert_eq!(frame.next(), Some(0));
    /// assert_eq!(frame.next(), None);
    /// ```
    fn defaults_like<T: Default>(&self) -> impl Frame<Item = T> {
        (0..self.len()).map(|_| T::default())
    }

    /// Creates a new frame with the same arity, filled with provided value.
    ///
    /// # Examples
    /// ```
    /// # use stoej_dsp_graph::frame::*;
    /// let template = frame![1, 2, 3];
    /// let mut frame = template.full_like(42);
    /// assert_eq!(frame.next(), Some(42));
    /// assert_eq!(frame.next(), Some(42));
    /// assert_eq!(frame.next(), Some(42));
    /// assert_eq!(frame.next(), None);
    /// ```
    fn full_like<T: Clone>(&self, val: T) -> impl Frame<Item = T> {
        (0..self.len()).map(move |_| val.clone())
    }
}

pub trait FrameOps: Frame + Sized
where
    Self::Item: Clone + Default,
{
    // ----- frame to frame -----

    fn cat<A: FrameOps<Item = Self::Item>>(self, other: A) -> impl FrameOps<Item = Self::Item> {
        let expected_len = self.len() + other.len();
        self.chain(other).expect_exact_size(expected_len)
    }

    fn dup(self) -> impl Frame<Item = Self::Item> {
        self.clone().cat(self)
    }

    fn slice(self, range: Range<usize>) -> impl Frame<Item = Self::Item> {
        self.skip(range.start).take(range.len())
    }

    fn move_to_front(self, range: Range<usize>) -> impl Frame<Item = Self::Item> {
        let (start, mid, end) = dissect(self, range);
        mid.cat(start).cat(end)
    }

    fn move_to_back(self, range: Range<usize>) -> impl Frame<Item = Self::Item> {
        let (start, mid, end) = dissect(self, range);
        start.cat(end).cat(mid)
    }

    fn discard(self, range: Range<usize>) -> impl Frame<Item = Self::Item> {
        let (start, _, end) = dissect(self, range);
        start.cat(end)
    }

    // ----- frame to stream -----

    /// Play frame from start to end, then emit silence
    fn play_once(self) -> impl Stream<Item = Self::Item> {
        PlayOnce(self)
    }

    /// Play frame from start to end, then hold final value
    fn play_and_hold(self) -> impl Stream<Item = Self::Item> {
        PlayAndHold::new(self)
    }

    /// Play frame repeatedly in a loop
    ///
    /// # Examples
    ///
    /// ```
    /// # use stoej_dsp_graph::frame::*;
    /// # use stoej_dsp_graph::stream::*;
    /// let frame = frame![1, 2, 3];
    /// let mut stream = frame.play_loop().unwrap();
    /// assert_eq!(stream.step(), 1);
    /// assert_eq!(stream.step(), 2);
    /// assert_eq!(stream.step(), 3);
    /// assert_eq!(stream.step(), 1);
    /// assert_eq!(stream.step(), 2);
    /// ```
    fn play_loop(self) -> Result<impl Stream<Item = Self::Item>> {
        PlayLoop::new(self)
    }
}

// ----- private trait methods -----

/// Splice a frame into three sections: start, middle and end.
fn dissect<A: FrameOps>(
    this: A,
    range: Range<usize>,
) -> (
    impl FrameOps<Item = A::Item>,
    impl FrameOps<Item = A::Item>,
    impl FrameOps<Item = A::Item>,
)
where
    A::Item: Clone + Default,
{
    // TODO: is there some clever way of reducing clones?
    let start = this.clone().slice(0..range.start);
    let mid = this.clone().slice(range.clone());
    let end = this.skip(range.end);
    (start, mid, end)
}

// ----- blanket impls -----
impl<T: DoubleEndedIterator + ExactSizeIterator + Clone> Frame for T {}
impl<T: Frame> FrameOps for T where T::Item: Clone + Default {}

// ===== PLAY ONCE =====

pub struct PlayOnce<F: FrameOps>(F)
where
    F::Item: Clone + Default;

impl<F: FrameOps> Iterator for PlayOnce<F>
where
    F::Item: Clone + Default,
{
    type Item = F::Item;

    fn next(&mut self) -> Option<Self::Item> {
        match self.0.next() {
            some @ Some(_) => some,
            None => Some(F::Item::default()),
        }
    }
}

// SAFETY: `None` values from inner iterator are always replaced with default()
unsafe impl<F: FrameOps> Stream for PlayOnce<F> where F::Item: Clone + Default {}

// ===== PLAY AND HOLD =====
pub struct PlayAndHold<F: FrameOps>
where
    F::Item: Clone + Default,
{
    upstream: F,
    last_val: F::Item,
}

impl<F: FrameOps> PlayAndHold<F>
where
    F::Item: Clone + Default,
{
    fn new(upstream: F) -> Self {
        Self {
            upstream,
            last_val: F::Item::default(),
        }
    }
}

impl<F: FrameOps> Iterator for PlayAndHold<F>
where
    F::Item: Clone + Default,
{
    type Item = F::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.last_val = self.upstream.next().unwrap_or(self.last_val.clone());
        Some(self.last_val.clone())
    }
}

// SAFETY: `None` values from inner iterator are always replaced with last value,
//         if last value is `None`, then `default()` is used.
unsafe impl<F: FrameOps> Stream for PlayAndHold<F> where F::Item: Clone + Default {}

// ===== PLAY LOOP =====

pub struct PlayLoop<F: FrameOps>(std::iter::Cycle<F>)
where
    F::Item: Clone + Default;

impl<F: FrameOps> PlayLoop<F>
where
    F::Item: Clone + Default,
{
    pub fn new(upstream: F) -> Result<Self> {
        ensure!(upstream.len() > 0, "cannot cycle an empty frame");
        Ok(Self(upstream.cycle()))
    }
}

impl<F: FrameOps> Iterator for PlayLoop<F>
where
    F::Item: Clone + Default,
{
    type Item = F::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

// SAFETY: struct fields are private. When creating a new instance through
//         the `new` method, it checks to ensure that frame is not empty
//         so that there is always at least one well-defined value to yield.
unsafe impl<F: FrameOps> Stream for PlayLoop<F> where F::Item: Clone + Default {}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn cat() {
        let mut frame = frame![1, 2].cat(frame![3, 4]);
        assert_eq!(frame.next(), Some(1));
        assert_eq!(frame.next(), Some(2));
        assert_eq!(frame.next(), Some(3));
        assert_eq!(frame.next(), Some(4));
        assert_eq!(frame.next(), None);
    }

    #[test]
    fn dup() {
        let mut frame = frame![1, 2].dup();
        assert_eq!(frame.next(), Some(1));
        assert_eq!(frame.next(), Some(2));
        assert_eq!(frame.next(), Some(1));
        assert_eq!(frame.next(), Some(2));
        assert_eq!(frame.next(), None);
    }
}
