//! Tools for "formal" validation. Formal in quotes because it's not really
//! formal.
//!
//! I took a class on formal validation and now I'm obsessed with safety.

use anyhow::{ensure, Result};

/// A bounded unsigned integer. Value should be within the half-open range
/// `MIN..MAX`.
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct BoundedU32<const MIN: u32, const MAX: u32>(u32);

impl<const MIN: u32, const MAX: u32> BoundedU32<MIN, MAX> {
    pub fn new(value: u32) -> Result<Self> {
        ensure!(MIN <= value && value < MAX, "value out of bounds");
        Ok(Self(value))
    }

    pub fn get(&self) -> u32 {
        self.0
    }

    pub fn maybe_set(&mut self, value: u32) -> Result<()> {
        ensure!(MIN <= value && value < MAX, "value out of bounds");
        self.0 = value;
        Ok(())
    }

    pub fn set(&mut self, value: u32) {
        assert!(MIN <= value && value < MAX, "value out of bounds");
        self.0 = value;
    }

    pub fn maybe_increment(&mut self) -> Result<()> {
        self.maybe_set(self.get() + 1)
    }

    pub fn increment(&mut self) {
        self.set(self.get() + 1);
    }

    pub fn maybe_decrement(&mut self) -> Result<()> {
        self.maybe_set(self.get() - 1)
    }

    pub fn decrement(&mut self) {
        self.set(self.get() - 1);
    }
}
