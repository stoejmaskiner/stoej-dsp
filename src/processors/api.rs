//! The main processor API, with all of its core operators.

use std::ops::{Index, IndexMut, Range};

use anyhow::{ensure, Result};

use crate::{
    buffers::api::{Buffer, BufferStorage},
    const_utils::{__DummyTrait, __max_usize},
};

extern crate static_assertions as sa;

/// A vertical slice of an audio buffer. One sample per channel.
///
/// Note that frames don't own their data.
pub struct Frame<'a>(&'a mut [f32]);

// public
impl<'a> Frame<'a> {
    pub fn new(data: &'a mut [f32]) -> Self {
        Self(data)
    }

    pub fn as_slice(&self) -> &[f32] {
        self.0
    }

    pub fn as_mut_slice(&mut self) -> &mut [f32] {
        self.0
    }

    pub unsafe fn get_unchecked(&self, index: usize) -> &f32 {
        self.0.get_unchecked(index)
    }

    pub fn split_at(&'a mut self, mid: usize) -> (Self, Self) {
        let (data1, data2) = self.0.split_at_mut(mid);
        (Self(data1), Self(data2))
    }

    pub fn slice(&mut self, from: usize, to: usize) -> Frame {
        Frame::new(&mut self.0[from..to])
    }

    /// Copy one range of items from one location of the frame to another.
    pub fn copy_within(&mut self, from: Range<usize>, to: usize) {
        self.0.copy_within(from, to);
    }

    /// Copy contents of another frame into this.
    ///
    /// This is different than [fill_from_iter] in that the two frames must
    /// have the same length.
    pub fn copy_from_frame(&mut self, other: &Frame) {
        self.0.copy_from_slice(other.0)
    }

    /// Copy one range of items from one frame to another frame.
    pub fn copy_range_from_frame(
        &mut self,
        other: &mut Frame,
        from: Range<usize>,
        to: usize,
    ) {
        self.slice(to, from.end - from.start + to)
            .copy_from_frame(&other.slice(from.start, from.end));
    }

    /// Copy into an owned array.
    ///
    /// Note that the size hint must be correct.
    pub fn to_owned<const N: usize>(&mut self) -> [f32; N] {
        let mut dst = [0.0f32; N];
        Frame::new(&mut dst).copy_from_frame(self);
        dst
    }

    /// Copy values at `indexes` into other frame
    pub fn swizzle(&self, other: &mut Frame, indexes: &[usize]) {
        for (dst, i) in other.iter_mut().zip(indexes) {
            *dst = self[*i];
        }
    }
}

impl<'a> Index<usize> for Frame<'a> {
    type Output = f32;

    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl<'a> IndexMut<usize> for Frame<'a> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[index]
    }
}

impl<'a> BufferStorage for Frame<'a> {
    fn iter(&self) -> impl Iterator<Item = &f32> {
        self.0.iter()
    }

    fn iter_mut(&mut self) -> impl Iterator<Item = &mut f32> {
        self.0.iter_mut()
    }

    fn len(&self) -> usize {
        self.0.len()
    }
}

impl<'a> Buffer for Frame<'a> {}

pub trait Processor: Sized {
    const I: usize;
    const O: usize;

    /// Call this whenever the sample rate changes.
    ///
    /// This method should not perform any expensive computations, and instead
    /// defers that to `init`. This method should pass the call to all its
    /// children.
    fn on_sr_changed(&mut self, sr: f32) -> Result<()>;

    /// Method-chaining version of [on_sr_changed]
    fn with_sr(mut self, sr: f32) -> Result<Self> {
        self.on_sr_changed(sr)?;
        Ok(self)
    }

    /// Resets internal state to default, cancelling envelopes, delay tails,
    /// etc...
    fn reset(&mut self);

    /// This should be implemented to derive all other trait functions.
    fn step(&mut self, in_frame: &mut Frame, out_frame: &mut Frame);

    /// This should be implemented to derive all other trait functions.
    fn step_replacing(&mut self, io_frame: &mut Frame);

    /// Report the tail length in samples, i.e. how long it takes for the output
    /// to decay to 0 after silence appeared at the input, minus the [latency].
    fn tail(&self) -> usize {
        0
    }

    /// Report the minimum latency in samples, i.e. how long it takes for the
    /// output to raise over 0 after the input raises over 0.
    fn latency(&self) -> usize {
        0
    }
}
