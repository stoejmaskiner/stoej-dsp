//! Basic processors that are foundational for developing larger prcessors.

use contracts::contract_trait;

use super::traits::{MonoProcessor, MultiChannelProcessor, Processor};

/// A processor that passes its inputs unchanged.
///
/// While useless on its own, it can be useful as a dummy processor in places
/// that expect a processor as a generic.
pub struct ProcIdentity<const N: usize>;

#[contract_trait]
impl<const N: usize> Processor for ProcIdentity<N> {
    const I: usize = N;
    const O: usize = N;

    fn step(&mut self, in_: &[f32], out: &mut [f32]) {
        out.copy_from_slice(in_);
    }

    fn step_replacing(&mut self, _io: &mut [f32]) {}
}

#[contract_trait]
impl<const N: usize> MultiChannelProcessor for ProcIdentity<N> {
    fn process_channel(&mut self, _chan: usize, in_: &[f32], out: &mut [f32]) {
        out.copy_from_slice(in_);
    }

    fn process_replacing_channel(&mut self, _chan: usize, _io: &mut [f32]) {}
}

/// Apply a fixed gain to any number of channels.
///
/// If you want per-sample smoothed gain or you want to do ring modulation,
/// use [`ProcMul`] instead.
pub struct ProcGain<const N: usize> {
    pub gain: f32,
}

#[contract_trait]
impl<const N: usize> Processor for ProcGain<N> {
    const I: usize = N;
    const O: usize = N;

    fn step(&mut self, in_: &[f32], out: &mut [f32]) {
        out.iter_mut()
            .zip(in_.iter())
            .for_each(|(o, i)| *o = *i * self.gain);
    }

    fn step_replacing(&mut self, io_: &mut [f32]) {
        io_.iter_mut().for_each(|x| *x *= self.gain);
    }
}

#[contract_trait]
impl<const N: usize> MultiChannelProcessor for ProcGain<N> {
    fn process_channel(&mut self, _chan: usize, in_: &[f32], out: &mut [f32]) {
        out.iter_mut()
            .zip(in_.iter())
            .for_each(|(o, i)| *o = *i * self.gain);
    }

    fn process_replacing_channel(&mut self, _chan: usize, io: &mut [f32]) {
        io.iter_mut().for_each(|x| *x *= self.gain);
    }
}

/// Multiply two channels together.
///
/// The replacing version stores the result in the first channel, and leaves
/// the second channel unchanged.
pub struct ProcMul;

#[contract_trait]
impl Processor for ProcMul {
    const I: usize = 2;
    const O: usize = 1;

    fn step(&mut self, in_: &[f32], out: &mut [f32]) {
        // TODO: force no bounds checks on indexing
        out[0] = in_[0] * in_[1];
    }

    fn step_replacing(&mut self, io_: &mut [f32]) {
        // TODO: force no bounds checks on indexing
        io_[0] *= io_[1];
    }
}

/// Add two channels together.
///
/// The replacing version stores the result in the first channel, and leaves
/// the second channel unchanged.
pub struct ProcAdd;

#[contract_trait]
impl Processor for ProcAdd {
    const I: usize = 2;
    const O: usize = 1;

    fn step(&mut self, in_: &[f32], out: &mut [f32]) {
        // TODO: force no bounds checks on indexing
        out[0] = in_[0] + in_[1];
    }

    fn step_replacing(&mut self, io_: &mut [f32]) {
        // TODO: force no bounds checks on indexing
        io_[0] += io_[1];
    }
}

/// Divide two channels together.
///
/// The replacing version stores the result in the first channel, and leaves
/// the second channel unchanged.
pub struct ProcDiv;

#[contract_trait]
impl Processor for ProcDiv {
    const I: usize = 2;
    const O: usize = 1;

    fn step(&mut self, in_: &[f32], out: &mut [f32]) {
        // TODO: force no bounds checks on indexing
        out[0] = in_[0] / in_[1];
    }

    fn step_replacing(&mut self, io_: &mut [f32]) {
        // TODO: force no bounds checks on indexing
        io_[0] /= io_[1];
    }
}

/// Subtract two channels together.
///
/// The replacing version stores the result in the first channel, and leaves
/// the second channel unchanged.
pub struct ProcSub;

#[contract_trait]
impl Processor for ProcSub {
    const I: usize = 2;
    const O: usize = 1;

    fn step(&mut self, in_: &[f32], out: &mut [f32]) {
        // TODO: force no bounds checks on indexing
        out[0] = in_[0] - in_[1];
    }

    fn step_replacing(&mut self, io_: &mut [f32]) {
        // TODO: force no bounds checks on indexing
        io_[0] -= io_[1];
    }
}

pub struct ProcClosure(pub fn(f32) -> f32);

#[contract_trait]
impl Processor for ProcClosure {
    const I: usize = 1;
    const O: usize = 1;

    fn step(&mut self, in_: &[f32], out: &mut [f32]) {
        out[0] = self.0(in_[0]);
    }

    fn step_replacing(&mut self, _io: &mut [f32]) {
        _io[0] = self.0(_io[0]);
    }
}

impl MonoProcessor for ProcClosure {
    fn step_mono(&mut self, x: f32) -> f32 {
        self.0(x)
    }
}
