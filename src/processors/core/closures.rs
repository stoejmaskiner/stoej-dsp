pub struct Apply<const I: usize, const O: usize>(fn(&[f32; I], &mut [f32; O]));

pub struct Fold<const I: usize>(fn(&[f32; I]) -> f32);

pub struct Map<const N: usize>(fn(f32) -> f32);
