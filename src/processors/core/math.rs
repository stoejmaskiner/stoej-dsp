pub struct Const<const N: usize>(pub [f32; N]);

pub struct Add;

pub struct Sum;

pub struct Sub;

pub struct Mul;

pub struct Prod;

pub struct Div;

pub struct Neg;

pub struct Mod;

/// Flushes subnormal numbers to zero and infinities to max
pub struct DenormalFilter;

/// Fills NaN values with a provided value
pub struct ReplaceNaN;

/// Holds last non-NaN value
pub struct HoldNaN;
