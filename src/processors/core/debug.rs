/// Panics on a NaN value.
pub struct AssertNotNaN;

/// Prints input frame info every N frames
pub struct Inspect<const N: usize>;

/// Prints input frame on condition
pub struct InspectIf<const I: usize>(fn(&[f32; I]) -> bool);
