use std::collections::HashSet;

use crate::buffers::api::BufferStorage;
use crate::const_utils::{__DummyTrait, __max_usize};

use super::super::api::{Frame, Processor};

use tinyvec::{Array, ArrayVec};

use anyhow::{ensure, Result};
pub trait PatchOps: Processor {
    fn parallel<PB: Processor>(self, other: PB) -> Parallel<Self, PB> {
        Parallel(self, other)
    }

    fn patch<PB: Processor, const N: usize>(
        self,
        other: PB,
        patches: [(usize, usize); N],
    ) -> Result<Patch<Self, PB, N>>
    where
        [(); Self::O - N]: __DummyTrait,
        [(); PB::I - N]: __DummyTrait,
    {
        Patch::new(self, other, patches)
    }

    fn chain<PB: Processor>(self, other: PB) -> Chain<Self, PB> {
        Chain(self, other)
    }
}

impl<P: Processor> PatchOps for P {}

pub struct Parallel<PA: Processor, PB: Processor>(pub PA, pub PB);

impl<PA: Processor, PB: Processor> Processor for Parallel<PA, PB>
where
    [(); PB::O]: __DummyTrait,
{
    const I: usize = PA::I + PB::I;
    const O: usize = PA::O + PB::O;

    fn step(&mut self, in_frame: &mut Frame, out_frame: &mut Frame) {
        debug_assert!(in_frame.len() >= Self::I);
        debug_assert!(out_frame.len() >= Self::O);
        self.0.step(
            &mut in_frame.slice(0, PA::I),
            &mut out_frame.slice(0, PA::O),
        );
        self.1.step(
            &mut in_frame.slice(PA::I, Self::I),
            &mut out_frame.slice(PA::O, Self::O),
        );
    }

    fn step_replacing(&mut self, io_frame: &mut Frame) {
        debug_assert!(io_frame.len() >= Self::I);
        debug_assert!(io_frame.len() >= Self::O);

        // TODO(rust-#76560): make `const` when Rust supports outer const items
        let max_width = __max_usize(PA::I, PA::O) + __max_usize(PB::I, PB::O);

        // handle edge-case: intermediate steps will not fit in the provided frame,
        // so we need to copy to a scratch buffer
        if max_width > __max_usize(Self::I, Self::O) {
            let mut scratch_space = [0.0f32; PB::O];
            let scratch_space = &mut Frame::new(&mut scratch_space);
            self.1
                .step(&mut io_frame.slice(PA::I, PA::I + PB::I), scratch_space);
            self.0.step_replacing(io_frame);
            io_frame.copy_range_from_frame(scratch_space, 0..PB::O, PA::O);
            return;
        }

        self.1.step_replacing(
            &mut io_frame.slice(PA::I, __max_usize(PA::I + PB::I, PA::I + PB::O)),
        );

        // shuffle around data so that proc A and B don't overwrite each other
        if PA::O > PA::I {
            io_frame.copy_within(PA::I..PA::I + PB::O, PA::O);
            self.0.step_replacing(io_frame);
        } else {
            self.0.step_replacing(io_frame);
            io_frame.copy_within(PA::I..PA::I + PB::O, PA::O);
        }
    }

    fn on_sr_changed(&mut self, sr: f32) -> Result<()> {
        // propagate sr change event
        self.0.on_sr_changed(sr)?;
        self.1.on_sr_changed(sr)?;
        Ok(())
    }

    fn reset(&mut self) {
        // propagate reset event
        self.0.reset();
        self.1.reset();
    }

    fn tail(&self) -> usize {
        self.0.tail().max(self.1.tail())
    }

    fn latency(&self) -> usize {
        self.0.latency().min(self.1.latency())
    }
}

/// A processor that chains two processors together, connecting as many ports as
/// possible
pub struct Chain<PA: Processor, PB: Processor>(pub PA, pub PB);

impl<PA: Processor, PB: Processor> Processor for Chain<PA, PB>
where
    [(); PA::O]: __DummyTrait,
{
    const I: usize = PA::I;

    const O: usize = PA::O;

    fn on_sr_changed(&mut self, sr: f32) -> Result<()> {
        // propagate sr change event
        self.0.on_sr_changed(sr)?;
        self.1.on_sr_changed(sr)?;
        Ok(())
    }

    fn reset(&mut self) {
        // propagate reset event
        self.0.reset();
        self.1.reset();
    }

    fn step(&mut self, in_frame: &mut Frame, out_frame: &mut Frame) {
        // TODO: `consteval if` equivalent, if Rust ever implements it...
        if PA::O > PB::O {
            let mut scratch_space = [0.0f32; PA::O];
            let scratch_space = &mut Frame::new(&mut scratch_space);
            self.0.step(in_frame, scratch_space);
            self.1.step(scratch_space, out_frame);
        } else {
            self.0.step(in_frame, out_frame);
            self.1.step_replacing(out_frame);
        }
    }

    fn step_replacing(&mut self, io_frame: &mut Frame) {
        // TODO: `consteval if` equivalent, if Rust ever implements it...
        if PA::O > __max_usize(PA::I, PB::O) {
            let mut scratch_space = [0.0f32; PA::O];
            let scratch_space = &mut Frame::new(&mut scratch_space);
            self.0.step(io_frame, scratch_space);
            self.1.step(scratch_space, io_frame);
        } else {
            self.0.step_replacing(io_frame);
            self.1.step_replacing(io_frame);
        }
    }

    fn tail(&self) -> usize {
        match (self.0.tail(), self.1.tail()) {
            (0, t) | (t, 0) => t,
            // note: assuming that processors are linear, this is the length of
            //       their convolution. Usually this is a decent approximation.
            (t1, t2) => t1 + t2 - 1,
        }
    }

    fn latency(&self) -> usize {
        self.0.latency() + self.1.latency()
    }
}

/// Create multiple patches between two processors.
pub struct Patch<PA: Processor, PB: Processor, const N: usize>
where
    [(); PA::O - N]: __DummyTrait,
    [(); PB::I - N]: __DummyTrait,
{
    pub pa: PA,
    pub pb: PB,

    patch_ao_to_bi: [(usize, usize); N],
    patch_si_to_bi: [(usize, usize); PB::I - N],
    patch_ao_to_so: [(usize, usize); PA::O - N],
}

impl<PA: Processor, PB: Processor, const N: usize> Patch<PA, PB, N>
where
    [(); PA::O - N]: __DummyTrait,
    [(); PB::I - N]: __DummyTrait,
{
    pub fn new(pa: PA, pb: PB, patches: [(usize, usize); N]) -> Result<Self> {
        for (po, pi) in patches {
            ensure!(po < PB::I, "patch out of range");
            ensure!(pi < PA::O, "patch out of range");
        }

        let (a, b): (Vec<_>, Vec<_>) = patches.iter().copied().unzip();
        let a = HashSet::<usize>::from_iter(a);
        let b = HashSet::<usize>::from_iter(b);
        ensure!(a.len() == N, "cannot patch one port into multiple ports");
        ensure!(b.len() == N, "cannot patch multiple ports into one port");

        let mut a_outs = Vec::from_iter(0..PA::O);
        let mut b_ins = Vec::from_iter(0..PB::I);

        for (a, b) in patches {
            let _ = a_outs.binary_search(&a).and_then(|idx| {
                a_outs.remove(idx);
                Ok(())
            });
            let _ = b_ins.binary_search(&b).and_then(|idx| {
                b_ins.remove(idx);
                Ok(())
            });
        }

        // sanity check
        debug_assert_eq!(a_outs.len(), PA::O - N);
        debug_assert_eq!(b_ins.len(), PB::I - N);

        let leftover_ins = PA::I..PA::I + PB::I - N;
        let leftover_outs = PB::O..PB::O + PA::O - N;

        Ok(Self {
            pa,
            pb,
            patch_ao_to_bi: patches,
            patch_si_to_bi: leftover_ins
                .zip(b_ins)
                .collect::<Vec<_>>()
                .try_into()
                .expect("infallible"),
            patch_ao_to_so: a_outs
                .into_iter()
                .zip(leftover_outs)
                .collect::<Vec<_>>()
                .try_into()
                .expect("infallible"),
        })
    }
}

impl<PA: Processor, PB: Processor, const N: usize> Processor for Patch<PA, PB, N>
where
    [(); PA::O - N]: __DummyTrait,
    [(); PB::I - N]: __DummyTrait,
    [(); __max_usize(PA::O, PB::I)]: __DummyTrait,
{
    const I: usize = PA::I + PB::I - N;

    const O: usize = PB::O + PA::O - N;

    fn on_sr_changed(&mut self, sr: f32) -> Result<()> {
        // propagate sr change event
        self.pa.on_sr_changed(sr)?;
        self.pb.on_sr_changed(sr)?;
        Ok(())
    }

    fn reset(&mut self) {
        // propagate reset event
        self.pa.reset();
        self.pb.reset();
    }

    fn step(&mut self, in_frame: &mut Frame, out_frame: &mut Frame) {
        let mut scratch_space_a = [0.0f32; __max_usize(PA::O, PB::I)];
        let scratch_space_a = &mut Frame::new(&mut scratch_space_a);

        let mut scratch_space_b = [0.0f32; __max_usize(PA::O, PB::I)];
        let scratch_space_b = &mut Frame::new(&mut scratch_space_b);

        self.pa.step(in_frame, scratch_space_a);

        for (a, b) in self.patch_ao_to_bi.iter() {
            scratch_space_b[*b] = scratch_space_a[*a];
        }

        for (i, b) in self.patch_si_to_bi.iter() {
            scratch_space_b[*b] = in_frame[*i];
        }

        self.pb.step(scratch_space_b, out_frame);

        for (a, b) in self.patch_ao_to_so.iter() {
            out_frame[*b] = scratch_space_a[*a];
        }
    }

    fn step_replacing(&mut self, io_frame: &mut Frame) {
        let mut scratch_space_a = [0.0f32; __max_usize(PA::O, PB::I)];
        let scratch_space_a = &mut Frame::new(&mut scratch_space_a);

        let mut scratch_space_b = [0.0f32; __max_usize(PA::O, PB::I)];
        let scratch_space_b = &mut Frame::new(&mut scratch_space_b);

        self.pa.step(io_frame, scratch_space_a);

        for (a, b) in self.patch_ao_to_bi.iter() {
            scratch_space_b[*b] = scratch_space_a[*a];
        }

        for (i, b) in self.patch_si_to_bi.iter() {
            scratch_space_b[*b] = io_frame[*i];
        }

        self.pb.step(scratch_space_b, io_frame);

        for (a, b) in self.patch_ao_to_so.iter() {
            io_frame[*b] = scratch_space_a[*a];
        }
    }

    fn tail(&self) -> usize {
        match (self.pa.tail(), self.pb.tail()) {
            (0, t) | (t, 0) => t,
            // note: assuming that processors are linear, this is the length of
            //       their convolution. Usually this is a decent approximation.
            (t1, t2) => t1 + t2 - 1,
        }
    }

    fn latency(&self) -> usize {
        self.pa.latency() + self.pb.latency()
    }
}

pub struct PatchInto<P: Processor, const I: usize>
where
    [usize; I]: Array<Item = usize>,
    [usize; P::I]: __DummyTrait,
{
    processor: P,
    patches: [usize; P::I],
    leftover_ins: ArrayVec<[usize; I]>,
}

impl<P: Processor, const I: usize> PatchInto<P, I>
where
    [usize; I]: Array<Item = usize>,
    [usize; P::I]: __DummyTrait,
{
    pub fn new(processor: P, patches: [usize; P::I]) -> Result<Self> {
        ensure!(I <= P::I, "too many inputs");

        Ok(Self {
            processor,
            patches,
            leftover_ins: ArrayVec::from_iter(
                (0..I).filter(|i| !patches.contains(i)),
            ),
        })
    }
}

// impl<P: Processor, const I: usize> Processor for PatchInto<P, I>
// where
//     [usize; I]: Array<Item = usize>,
// {
//     const I: usize = I;
//     const O: usize = P::O;

//     fn on_sr_changed(&mut self, sr: f32) -> Result<()> {
//         todo!()
//     }

//     fn reset(&mut self) {
//         todo!()
//     }

//     fn step(&mut self, in_frame: &mut Frame, out_frame: &mut Frame) {
//         todo!()
//     }

//     fn step_replacing(&mut self, io_frame: &mut Frame) {
//         todo!()
//     }
// }
