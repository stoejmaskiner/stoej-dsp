//! Low-level memory management utilities, these are used to break the strict
//! rules of the framework, when they become too limiting. Use them with care.
//!
//! Use cases include:
//! - creating efficient feedback loops
//! - send values from deep within processor graphs
//! - implementing complex stateful algorithms
//! - you hate declarative programming

use std::{cell::Cell, marker::PhantomData};

use anyhow::{ensure, Result};

use crate::{buffers::BufferStorage, validation_utils::BoundedU32};

use super::super::api::{Frame, Processor};

/// A variable is an opaque container for a value. Accessing the value is
/// possible only through [Read] and [Write] processors.
///
/// Writers are reference counted such that there may only ever be one.
#[derive(Debug)]
pub struct Variable<const N: usize> {
    data: [f32; N],
    ref_count: Cell<BoundedU32<0, 2>>,
}

impl<const N: usize> Variable<N> {
    pub fn new() -> Self {
        Self {
            data: [0.0; N],
            ref_count: Cell::new(BoundedU32::new(0).expect("infallible")),
        }
    }

    pub fn write<'a>(&'a self) -> Result<Write<'a, N>> {
        ensure!(
            self.ref_count.get().get() == 0,
            "cannot instantiate writer twice"
        );
        let mut new_ref_count = self.ref_count.get();
        new_ref_count.increment();
        self.ref_count.set(new_ref_count);
        Ok(Write::<N> {
            phantom: PhantomData,
            data_ref: self as *const Self as *mut Self,
        })
    }

    pub fn read<'a>(&'a self) -> Read<'a, N> {
        Read { data_ref: self }
    }
}

#[derive(Debug)]
pub struct Write<'a, const N: usize> {
    phantom: PhantomData<&'a Variable<N>>,
    data_ref: *mut Variable<N>,
}

impl<const N: usize> Drop for Write<'_, N> {
    fn drop(&mut self) {
        unsafe {
            assert_eq!((*self.data_ref).ref_count.get().get(), 1);
            (*self.data_ref).ref_count.get_mut().decrement();
            assert_eq!((*self.data_ref).ref_count.get().get(), 0);
        }
    }
}

impl<const N: usize> Processor for Write<'_, N> {
    const I: usize = N;
    const O: usize = 0;

    fn step(&mut self, in_frame: &mut Frame, _out_frame: &mut Frame) {
        debug_assert_eq!(_out_frame.len(), 0);
        debug_assert_eq!(in_frame.len(), Self::I);
        unsafe {
            (*self.data_ref).data = in_frame.to_owned();
        }
    }

    fn on_sr_changed(&mut self, _sr: f32) -> anyhow::Result<()> {
        Ok(())
    }

    fn reset(&mut self) {}

    fn step_replacing(&mut self, io_frame: &mut Frame) {
        debug_assert_eq!(io_frame.len(), Self::I);
        unsafe {
            (*self.data_ref).data = io_frame.to_owned();
        }
    }
}

#[derive(Debug)]
pub struct Read<'a, const N: usize> {
    data_ref: &'a Variable<N>,
}

impl<const N: usize> Processor for Read<'_, N> {
    const I: usize = 0;
    const O: usize = N;

    fn on_sr_changed(&mut self, _sr: f32) -> anyhow::Result<()> {
        Ok(())
    }

    fn reset(&mut self) {}

    fn step(&mut self, _in_frame: &mut Frame, out_frame: &mut Frame) {
        debug_assert_eq!(_in_frame.len(), 0);
        debug_assert_eq!(out_frame.len(), Self::O);
        out_frame.fill_from_iter((*self.data_ref).data.iter().copied());
    }

    fn step_replacing(&mut self, io_frame: &mut Frame) {
        debug_assert_eq!(io_frame.len(), Self::O);
        io_frame.fill_from_iter((*self.data_ref).data.iter().copied());
    }
}
