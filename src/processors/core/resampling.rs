use super::super::api::Processor;

/// A raw upsampling processor, that wraps an inner processor and provides it
/// with a higher sampling rate.
///
/// The upsampler simply pads each sample with zeros. All anti-aliasing must
/// be done by the inner processor.
pub struct FixedUpsampling<P: Processor, const N: usize>(P);

/// A raw downsampling processor, that wraps an inner processor and provides it
/// with a lower sampling rate.
///
/// The downsampler simply discards samples. All anti-aliasing must
/// be done by the user or the inner processor.
pub struct FixedDownsampling<P: Processor, const N: usize>(P);
