/// Feed-forward unit delay
pub struct UnitDelay;

/// Fixed-length delay
pub struct FixedDelay<const D: usize>;

/// A fixed delay tap, linked to a [`FixedDelay`]
pub struct DelayTap;
