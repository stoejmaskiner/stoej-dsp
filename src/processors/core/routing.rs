use anyhow::{Ok, Result};

use crate::buffers::BufferStorage;

use super::super::api::{Frame, Processor};

pub struct Identity;

// TODO: move to library
/// An empty processor, takes no input and outputs nothing. Does nothing.
pub type Empty = Discard<0>;

/// Takes any frame and discards it, useful if outputs are unused.
pub struct Discard<const N: usize>;

impl<const N: usize> Processor for Discard<N> {
    const I: usize = N;

    const O: usize = 0;

    fn on_sr_changed(&mut self, _sr: f32) -> Result<()> {
        Ok(())
    }

    fn reset(&mut self) {}

    fn step(&mut self, in_frame: &mut Frame, out_frame: &mut Frame) {
        debug_assert_eq!(in_frame.len(), Self::I);
        debug_assert_eq!(out_frame.len(), Self::O);
    }

    fn step_replacing(&mut self, io_frame: &mut Frame) {
        debug_assert_eq!(io_frame.len(), Self::I);
    }
}

/// Container for N alternative chains, activation depends on index input
pub struct Route;

/// Same as [`Route`], but only between 2 chains, and lets you pass a closure
/// to define the condition
pub struct RouteIf;

/// Apply the same processor multiple times
pub struct Repeat;

/// Picks between N inputs
pub struct Choose;

/// Picks between two inpts based on closure
pub struct ChooseIf;

/// Splits frame into `C` duplicate copies
pub struct Split;

/// Flips input frame upside-down
pub struct FlipFrame;

/// Creates N parallel instances of a processor
pub struct Replicate;
