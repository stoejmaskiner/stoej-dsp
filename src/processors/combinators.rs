//! Processor combinators create new processors by combining other processors

use contracts::contract_trait;

use super::traits::{MultiChannelProcessor, Processor};

/// Create a new processor by running two processors in parallel.
///
/// This is most commonly useful for creating a stereo processor from two
/// [`MonoProcessor`]s, in this case, it auto-implements
/// [`MultiChannelProcessor`].
///
/// Combining [`MonoProcessor`]s or [`MultiChannelProcessor`]s always results in
/// a [`MultiChannelProcessor`].
pub struct ProcParallel<A: Processor, B: Processor>(pub A, pub B);

#[contract_trait]
impl<A: Processor, B: Processor> Processor for ProcParallel<A, B> {
    const I: usize = A::I + B::I;
    const O: usize = A::O + B::O;

    fn step(&mut self, in_: &[f32], out: &mut [f32]) {
        self.0.step(&in_[0..A::I], &mut out[0..A::O]);
        self.1.step(&in_[A::I..Self::I], &mut out[A::O..Self::O]);
    }

    fn step_replacing(&mut self, io: &mut [f32]) {
        self.0.step_replacing(&mut io[0..A::I]);
        self.1.step_replacing(&mut io[A::I..Self::I]);
    }
}

#[contract_trait]
impl<A: MultiChannelProcessor, B: MultiChannelProcessor> MultiChannelProcessor
    for ProcParallel<A, B>
{
    fn process_channel(&mut self, chan: usize, in_: &[f32], out: &mut [f32]) {
        if chan < A::I {
            self.0.process_channel(chan, in_, out);
        } else {
            self.1.process_channel(chan - A::I, in_, out);
        }
    }

    fn process_replacing_channel(&mut self, chan: usize, io: &mut [f32]) {
        if chan < A::I {
            self.0.process_replacing_channel(chan, io);
        } else {
            self.1.process_replacing_channel(chan - A::I, io);
        }
    }
}

pub struct ProcPatch<A: Processor, B: Processor, const FROM: usize, const TO: usize>(
    pub A,
    pub B,
);
