//! Processor API
//!
//! There are a few traits here, the basic one is `Processor`, and some
//! additional traits are provided which can be used for special cases of
//! `Processor`, to make them more ergonomic or more optimized.
//!
//! ## Terminology
//! The docs for this module are particularly dense, there are a few terms here
//! that have very specific meanings.
//! - "step" the smallest processing stage, advances the processors by one unit
//!   of time.
//! - "frame" a slice containing all the inputs to a single step of a processor.
//! - "buffer" a slice containing a piece of a signal, could be a 1D buffer, in
//!   which case it is a sequence of numbers over time; or it could be 2D, in
//!   which case it is a sequence of frames over time.
//! - "length-wise" some transformation that is applied to an entire buffer,
//!   possibly enabling vectorization over multiple samples in time.
//! - "width-wise" some transformation that is applied to an entire frame,
//!   possibly enabling vectorization over multiple samples in the frame (but a
//!   single sample in time).

use contracts::{contract_trait, debug_requires};

use num::FromPrimitive;

/// A mapping from `I` input values to `O` output values, which may have a
/// hidden internal state. The main abstraction for manipulating signals.
///
/// Processors operate on signals one step at a time, consuming a full frame
/// of input on each step (that is one value for each input) and producing a
/// full frame of output at each step. In some specific cases, namely where
/// there are N inputs and N outputs, each processed independently and
/// pair-wise, the [MultiChannelProcessor] trait may be faster, but it cannot
/// always be implemented.
///
/// The internal state could be anything, the trait does not directly interact
/// with the state at all. Any type can be used to represent the state.
///
/// If you intend on implementing `Processor`, remember to apply the
/// `#[contract_trait]` attribute to the `impl` block.
///
/// Because of Rust's lackluster support of const generics and static
/// assertions, the code is severely lacking in validation, and there are
/// several pre-conditions and invariants which must be upheld manually. Doing
/// so at run-time would be too slow for the kind of low-latency tasks that DSP
/// requires. Some debug assertions are provided through the
/// [contracts](https://docs.rs/contracts/latest/contracts/) crate, but they
/// are not exhaustive. Make sure to read the docs on each function carefully
/// before implemeting it.
///
/// Most methods are optional, but consider implementing them anyways, as you
/// can likely get more optimized code out of them, if you implement them for
/// your specific processor.
#[contract_trait]
pub trait Processor: Sized {
    const I: usize;
    const O: usize;

    /// Process a sequence of frames in-place.
    #[debug_requires(io[0].len() == Self::I.max(Self::O))]
    fn process_replacing(&mut self, io: &mut [&mut [f32]]) {
        for frame in &mut *io {
            self.step_replacing(frame);
        }
    }

    /// Process a sequence of frames, copying the resulting frames in an output
    /// buffer.
    #[debug_requires(in_[0].len() == Self::I)]
    #[debug_requires(out[0].len() == Self::O)]
    #[debug_requires(in_.len() == out.len())]
    fn process(&mut self, in_: &[&[f32]], out: &mut [&mut [f32]]) {
        for (in_frame, out_frame) in in_.iter().zip(out.iter_mut()) {
            self.step(in_frame, out_frame);
        }
    }

    /// Process a single frame, copying the resulting frames in an output frame.
    #[debug_requires(in_.len() == Self::I)]
    #[debug_requires(out.len() == Self::O)]
    fn step(&mut self, in_: &[f32], out: &mut [f32]);

    /// Process a single frame in-place.
    #[debug_requires(io.len() == Self::I.max(Self::O))]
    fn step_replacing(&mut self, io: &mut [f32]);

    /// Use this to run expensive pre-computations on demand.
    fn initialize(&mut self) {}

    /// Reset the internal state to default, flush buffers and reset envelopes.
    fn reset(&mut self) {}

    /// Latency in fractions of samples. If you implement this, then
    /// [rounded_latency] is defined by default in terms of this.
    ///
    /// This latency can usually be calculated in terms of the exact latency of
    /// inner processors.
    fn exact_latency(&self) -> f32 {
        0.0
    }

    /// Latency in samples, rounded to the nearest integer.
    ///
    /// > Do not implement this! Instead, implement [exact_latency], which is
    /// > used by the default implementation of this method.
    fn rounded_latency(&self) -> usize {
        usize::from_f32(self.exact_latency().max(0.0).round())
            .expect("couldn't convert f32 latency to usize")
    }

    /// Tail length in fractions of samples. If you implement this, then
    /// [rounded_tail] is defined by default in terms of this.
    ///
    /// If tail length is finite, this should be the exact tail length, if it's
    /// infinite it should be the length until the tail decays below the
    /// smallest normal f32 value.
    ///
    /// This tail length can usually be calculated in terms of the exact tail of
    /// inner processors.
    fn exact_tail(&self) -> f32 {
        0.0
    }

    /// Tail length in samples, rounded to the nearest integer.
    ///
    /// > Do not implement this! Instead, implement [exact_tail], which is
    /// > used by the default implementation of this method.
    fn rounded_tail(&self) -> usize {
        usize::from_f32(self.exact_tail().max(0.0).ceil())
            .expect("couldn't convert f32 tail length to usize")
    }
}

/// A sub-type of Processor that alwyas has 1 in and 1 out.
///
/// It's generally less abstract than Processor so it might be slightly faster.
pub trait MonoProcessor: Processor {
    fn step_mono(&mut self, x: f32) -> f32;

    fn process_mono(&mut self, in_: &[f32], out: &mut [f32]) {
        out.copy_from_slice(in_);
        self.process_replacing_mono(out);
    }

    fn process_replacing_mono(&mut self, io: &mut [f32]) {
        io.iter_mut().for_each(|x| *x = self.step_mono(*x));
    }
}

/// A sub-type of Processor that always has N in and N out, matched pairwise.
///
/// Each parallel channel is independent of the others, so it is possible to
/// process an entire buffer at a time for each channel individually, which may
/// enable more vectorizations.
///
/// Note that it isn't always possible to implement this trait, if there is a
/// relationship between each input and or output, they must be processed one
/// frame at a time.
///
/// See also [ProcParallel] for an easy way of making `MultiChannelProcessor`s
/// without having to implement the trait manually.
#[contract_trait]
pub trait MultiChannelProcessor: Processor {
    #[debug_requires(chan < Self::I && chan < Self::O, "channel index out of range")]
    fn process_channel(&mut self, chan: usize, in_: &[f32], out: &mut [f32]);

    #[debug_requires(chan < Self::I && chan < Self::O, "channel index out of range")]
    fn process_replacing_channel(&mut self, chan: usize, io: &mut [f32]);
}

#[contract_trait]
impl<P: MonoProcessor> MultiChannelProcessor for P {
    fn process_channel(&mut self, _chan: usize, in_: &[f32], out: &mut [f32]) {
        self.process_mono(in_, out);
    }

    fn process_replacing_channel(&mut self, _chan: usize, io: &mut [f32]) {
        self.process_replacing_mono(io);
    }
}
