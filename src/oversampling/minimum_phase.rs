use array_macro::array;
use contracts::{contract_trait, debug_requires, requires};

use crate::{
    const_utils::{__DummyTrait, __max_usize},
    iir_filters::{
        design::{butter, FilterTopology::LowPass},
        processors::CascadedBiquad,
    },
    processors::{MonoProcessor, Processor},
};

/// Minimum phase 2x oversmapling
///
/// Const parameters `I_INNER` and `O_INNER` cannot at the moment be inferred
/// automatically because of Rust's awful const evaluation rules.
#[derive(Debug, Clone)]
pub struct MinPhase2xOS<const I_INNER: usize, const O_INNER: usize, P: Processor>
where
    [[(); __max_usize(I_INNER, O_INNER)]; 2]: __DummyTrait,
{
    pub inner_processor: P,
    up_filter: [CascadedBiquad<4>; I_INNER],
    down_filter: [CascadedBiquad<4>; O_INNER],
    buffer: [[f32; __max_usize(I_INNER, O_INNER)]; 2],
}

impl<const I_INNER: usize, const O_INNER: usize, P: Processor>
    MinPhase2xOS<I_INNER, O_INNER, P>
where
    [[(); __max_usize(I_INNER, O_INNER)]; 2]: __DummyTrait,
{
    #[requires(I_INNER == P::I)]
    #[requires(O_INNER == P::O)]
    pub fn new(inner_processor: P) -> anyhow::Result<Self> {
        Ok(Self {
            inner_processor,
            up_filter: array![CascadedBiquad::from_zpk(butter(
                8,
                (0.1, 0.1),
                LowPass,
                4.0,
            )?)?; I_INNER],
            down_filter: array![CascadedBiquad::from_zpk(butter(
                8,
                (0.1, 0.1),
                LowPass,
                4.0,
            )?)?; O_INNER],
            buffer: [[0.0; __max_usize(I_INNER, O_INNER)]; 2],
        })
    }
}

#[contract_trait]
impl<const I_INNER: usize, const O_INNER: usize, P: Processor> Processor
    for MinPhase2xOS<I_INNER, O_INNER, P>
where
    [[(); __max_usize(I_INNER, O_INNER)]; 2]: __DummyTrait,
{
    const I: usize = P::I;
    const O: usize = P::O;

    #[debug_requires(in_.len() == Self::I)]
    #[debug_requires(out.len() == Self::O)]
    fn step(&mut self, in_: &[f32], out: &mut [f32]) {
        // odd step up
        in_.iter()
            .zip(self.buffer[0].iter_mut())
            .zip(self.up_filter.iter_mut())
            .for_each(|((i, b), f)| {
                *b = f.step_mono(*i);
            });

        // even step up
        self.buffer[1]
            .iter_mut()
            .zip(self.up_filter.iter_mut())
            .for_each(|(b, f)| {
                *b = f.step_mono(0.0);
            });

        self.inner_processor.step_replacing(&mut self.buffer[0]);
        self.inner_processor.step_replacing(&mut self.buffer[1]);

        // odd step down, sample into output
        out.iter_mut()
            .zip(self.buffer[0].iter_mut())
            .zip(self.down_filter.iter_mut())
            .for_each(|((o, b), f)| {
                *o = f.step_mono(*b);
            });

        // even step down, discard
        self.buffer[1]
            .iter_mut()
            .zip(self.down_filter.iter_mut())
            .for_each(|(b, f)| {
                let _ = f.step_mono(*b);
            });
    }

    #[debug_requires(io.len() == Self::I.max(Self::O))]
    fn step_replacing(&mut self, io: &mut [f32]) {}
}
