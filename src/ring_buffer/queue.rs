use crate::const_utils::__DummyTrait;

use super::Base;

#[derive(Clone, Debug, Default)]
pub struct Queue<const E: usize>
where
    [(); 1 << E]: __DummyTrait,
{
    data: Base<E>,
    tail: usize,
    head: usize,
}

impl<const E: usize> Queue<E>
where
    [(); 1 << E]: __DummyTrait,
{
    pub fn new() -> Self {
        Self {
            data: Base::new(),
            tail: 0,
            head: 0,
        }
    }

    pub fn capacity(&self) -> usize {
        self.data.capacity()
    }

    pub fn as_slice(&self) -> &[f32] {
        return self.data.as_slice();
    }

    pub fn as_mut_slice(&mut self) -> &mut [f32] {
        return self.data.as_mut_slice();
    }

    pub fn clear(&mut self) {
        self.data.clear();
        self.head = 0;
        self.tail = 0;
    }

    /// Get the length of the queue
    ///
    /// # Caveats
    /// On a corrupted queue, this will return nonsensical results, as of now,
    /// it just returns 0.
    pub fn len(&self) -> usize {
        debug_assert!(
            self.head >= self.tail,
            "can't calculate negative length of a queue, the queue is corrupted"
        );
        self.head.saturating_sub(self.tail)
    }

    /// Checks whether the queue has underflowed or overflowed.
    ///
    /// A corrupted queue happens as a result of running unchecked operations
    /// which result in the queue having negative length or having a length
    /// higher than its capacity. Corrupted queues don't exhibit any unsafe
    /// behavior, but they behave out of spec, returning garbage data or
    /// overwriting values.
    ///
    /// A queue can return to being uncorrupted if it shrinks or grows back to
    /// a valid size. Keep in mind that the items still in the queue may still
    /// be incorrect, until they all have been replaced by fresh values.
    pub fn is_corrupted(&mut self) -> bool {
        self.head >= self.tail || self.len() > self.capacity()
    }

    /// Checks whether the queue is full.
    ///
    /// An overflown corrupted queue is also considered full, but an underflown
    /// queue is not.
    pub fn full(&self) -> bool {
        self.len() >= self.capacity()
    }

    /// Checks whether the queue is empty.
    ///
    /// An underflown corrupted queue is also considered empty.
    pub fn empty(&self) -> bool {
        self.len() <= 0
    }

    /// Enqueue and dequeue in one operation
    ///
    /// This doesn't change the size of the queue and thus doesn't need to do
    /// any checks, making it much faster than doing the enqueue and dequeue
    /// operations separately.
    ///
    /// Note that if the queue was already corrupted, this will give wrong
    /// results.
    pub fn rotate(&mut self, val: f32) -> f32 {
        let ret = self.dequeue_unchecked();
        self.enqueue_unchecked(val);
        ret
    }

    /// Fast enqueue, that may corrupt the queue if used incorrectly!
    ///
    /// If the queue is full, this corrupts the queue, overflowing it and
    /// overwriting existing data. This is safe, in Rust terms, but may be
    /// unwanted.
    pub fn enqueue_unchecked(&mut self, val: f32) {
        self.data.set_branchless(self.head, val);
        self.head += 1;
    }

    /// Fast dequeue, that may corrupt the queue if used incorrectly!
    ///
    /// If the queue is empty, this corrupts the queue, underflowing it and
    /// overwriting existing data. This is safe, in Rust terms, but may be
    /// unwanted.
    pub fn dequeue_unchecked(&mut self) -> f32 {
        let ret = self.data.get_branchless(self.tail);
        self.tail += 1;
        ret
    }

    pub fn enqueue(&mut self, val: f32) -> Result<(), ()> {
        if self.full() {
            return Err(());
        }
        self.enqueue_unchecked(val);
        Ok(())
    }

    pub fn dequeue(&mut self) -> Option<f32> {
        if self.empty() {
            return None;
        }
        Some(self.dequeue_unchecked())
    }

    pub fn dequeue_or(&mut self, alt: f32) -> f32 {
        if self.empty() {
            return alt;
        }
        self.dequeue_unchecked()
    }

    pub fn drain(&mut self) -> Drain<E> {
        Drain(self)
    }

    /// Fill queue from iterator, until full or until the iterator is empty
    pub fn fill(&mut self, iter: impl Iterator<Item = f32>) {
        for x in iter {
            if let Err(()) = self.enqueue(x) {
                break;
            }
        }
    }

    /// Fill queue from iterator, until the interator is empty, without
    /// checking for overflows
    pub fn fill_unchecked(&mut self, iter: impl Iterator<Item = f32>) {
        for x in iter {
            self.enqueue_unchecked(x);
        }
    }
}

pub struct Drain<'a, const E: usize>(&'a mut Queue<E>)
where
    [(); 1 << E]: __DummyTrait;

impl<'a, const E: usize> Iterator for Drain<'a, E>
where
    [(); 1 << E]: __DummyTrait,
{
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.dequeue()
    }
}
