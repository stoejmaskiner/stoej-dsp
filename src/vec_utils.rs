//! Internal vector utilities, make math with `std::Vec` a bit less painful

use std::ops::{Add, Div, Mul, Sub};

use nalgebra::ComplexField;
use num::traits::One;

pub trait VecMaps<T>: Sized {
    /// greedy map
    fn map<F: Fn(&T) -> T>(&self, f: F) -> Self;

    /// in-place map
    fn apply<F: Fn(T) -> T>(&mut self, f: F);

    /// binary map
    fn bin_map<F: Fn(&T, &T) -> T>(&self, other: &Self, f: F) -> Self;

    /// in-place binary map
    fn bin_apply<F: Fn(T, T) -> T>(&mut self, other: &Self, f: F);
}

pub trait VecOps<T>: VecMaps<T>
where
    for<'a> &'a T:
        Add<Output = T> + Mul<Output = T> + Div<Output = T> + Sub<Output = T>,
    T: One
        + Add<Output = T>
        + Mul<Output = T>
        + Div<Output = T>
        + Sub<Output = T>
        + Clone,
{
    /// element-wise addition
    fn el_add(&self, other: &Self) -> Self {
        self.bin_map(other, |a, b| a + b)
    }

    /// element-wise addition in-place
    fn el_mut_add(&mut self, other: &Self) {
        self.bin_apply(other, |a, b| a + b);
    }

    /// vector-scalar addition
    fn scalar_add(&self, other: T) -> Self {
        self.map(|x| x + &other)
    }

    /// vector-scalar addition in-place
    fn scalar_mut_add(&mut self, other: T) {
        self.apply(|x| x + other.clone());
    }

    /// element-wise addition
    fn el_sub(&self, other: &Self) -> Self {
        self.bin_map(other, |a, b| a - b)
    }

    /// element-wise addition in-place
    fn el_mut_sub(&mut self, other: &Self) {
        self.bin_apply(other, |a, b| a - b);
    }

    /// vector-scalar addition
    fn scalar_sub(&self, other: T) -> Self {
        self.map(|x| x - &other)
    }

    /// vector-scalar addition in-place
    fn scalar_mut_sub(&mut self, other: T) {
        self.apply(|x| x - other.clone());
    }

    /// element-wise multiplication
    fn el_mul(&self, other: &Self) -> Self {
        self.bin_map(other, |a, b| a * b)
    }

    /// element-wise multiplication in-place
    fn el_mut_mul(&mut self, other: &Self) {
        self.bin_apply(other, |a, b| a * b);
    }

    /// vector-scalar multiplication
    fn scalar_mul(&self, other: T) -> Self {
        self.map(|x| x * &other)
    }

    /// vector-scalar multiplication in-place
    fn scalar_mut_mul(&mut self, other: T) {
        self.apply(|x| x * other.clone());
    }

    /// element-wise reciprocal
    fn el_recip(&self) -> Self {
        self.map(|x| &T::one() / x)
    }

    /// element-wise reciprocal in-place
    fn el_mut_recip(&mut self) {
        self.apply(|x| T::one() / x);
    }
}

pub trait VecFuncs<T>: VecMaps<T> {
    fn sqrt(&self) -> Self;
    fn powi(&self, n: i32) -> Self;
}

impl<T> VecMaps<T> for Vec<T>
where
    T: Clone,
{
    fn map<F: Fn(&T) -> T>(&self, f: F) -> Self {
        // TODO: faster wih loops?
        self.iter().map(f).collect()
    }

    fn apply<F: Fn(T) -> T>(&mut self, f: F) {
        for i in 0..self.len() {
            self[i] = f(self[i].clone());
        }
    }

    fn bin_map<F: Fn(&T, &T) -> T>(&self, other: &Self, f: F) -> Self {
        self.iter().zip(other).map(|(a, b)| f(a, b)).collect()
    }

    fn bin_apply<F: Fn(T, T) -> T>(&mut self, other: &Self, f: F) {
        for (a, b) in self.iter_mut().zip(other.iter()) {
            *a = f((*a).clone(), (*b).clone());
        }
    }
}

impl<T> VecOps<T> for Vec<T>
where
    for<'a> &'a T:
        Add<Output = T> + Mul<Output = T> + Div<Output = T> + Sub<Output = T>,
    T: One
        + Add<Output = T>
        + Mul<Output = T>
        + Div<Output = T>
        + Sub<Output = T>
        + Clone,
{
}

impl<T: Clone + ComplexField> VecFuncs<T> for Vec<T> {
    fn sqrt(&self) -> Self {
        self.map(|x| x.clone().sqrt())
    }

    fn powi(&self, n: i32) -> Self {
        self.map(|x| x.clone().powi(n))
    }
}
