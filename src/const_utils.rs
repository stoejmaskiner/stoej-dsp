//! Utilities to overcome Rust's half-assed const generics and const fn
//! implementations

/// A marker trait that does nothing but is implemented for all types. It is
/// sometimes required in where clauses, because of how unstable const generic
/// expressions validate types.
#[doc(hidden)]
pub trait __DummyTrait {}

impl<T> __DummyTrait for T {}

#[doc(hidden)]
#[must_use]
pub const fn __max_usize(a: usize, b: usize) -> usize {
    if a > b {
        a
    } else {
        b
    }
}

#[doc(hidden)]
#[must_use]
pub const fn __min_usize(a: usize, b: usize) -> usize {
    if a < b {
        a
    } else {
        b
    }
}
