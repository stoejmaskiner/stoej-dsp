use super::tf::TF;
use anyhow::{Context, Result};
use num::complex::Complex64;
use rust_poly::Get;

#[derive(PartialEq, Debug, Clone)]
pub struct ZPK {
    /// Zeros
    pub z: Vec<Complex64>,

    /// Poles
    pub p: Vec<Complex64>,

    /// System gain
    pub k: Complex64,
}

impl ZPK {
    pub fn from_tf(tf: TF) -> Result<Self> {
        let TF { b, a } = tf.normalize()?;

        // TODO: explain this, are you making it monic?
        let a_0 = a.get(-1isize).context("index out of range")?;
        let b = b / a_0;
        let a = a / a_0;

        // isolate the system gain to a separate variable
        let k = b.get(-1isize).context("index out of range")?;

        // normalize the system gain to unity
        let b = b / k;

        let z = b.roots();
        let p = a.roots();
        Ok(Self { z, p, k })
    }
}

impl TryFrom<TF> for ZPK {
    type Error = anyhow::Error;

    fn try_from(value: TF) -> Result<Self> {
        Self::from_tf(value)
    }
}

#[cfg(test)]
mod test {
    use rust_poly::{complex, poly};

    use super::*;

    #[test]
    fn test_from_tf() {
        let lhs = ZPK::from_tf(TF {
            b: poly![4.0, 1.0],
            a: poly![8.0, 2.0],
        })
        .unwrap();
        let rhs = ZPK {
            z: vec![complex!(-4.0, 0.0)],
            p: vec![complex!(-4.0, 0.0)],
            k: complex!(0.5, 0.0),
        };
        assert_eq!(lhs, rhs);
    }
}
