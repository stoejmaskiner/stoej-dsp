use anyhow::{bail, Context, Result};

use num::Zero;
use rust_poly::{Get, Poly, Poly64};

use super::ZPK;

#[derive(PartialEq, Debug)]
pub struct TF {
    pub b: Poly64,
    pub a: Poly64,
}

impl TF {
    /// Normalize a transfer function
    pub fn normalize(&self) -> Result<Self> {
        let (num, den) = (&self.b, &self.a);
        if den.is_zero() {
            bail!("Transfer function has zero as a denominator, cannot divide by zero!");
        }

        Ok(Self {
            b: num / den.get(-1isize).context("index out of bounds")?,
            a: den / den.get(-1isize).context("index out of bounds")?,
        })
    }

    fn from_zpk(value: ZPK) -> Self {
        let b = Poly::from_roots(&value.z) * value.k;
        let a = Poly::from_roots(&value.p);
        Self { b, a }
    }
}

impl From<ZPK> for TF {
    fn from(value: ZPK) -> Self {
        Self::from_zpk(value)
    }
}

#[cfg(test)]
mod test {
    use rust_poly::{complex, poly};

    use crate::iir_filters::coeffs::ZPK;

    use super::{Zero, TF};

    #[test]
    #[should_panic(
        expected = "called `Result::unwrap()` on an `Err` value: Transfer function has zero as a denominator, cannot divide by zero!"
    )]
    fn test_normalize_div_by_zero() {
        TF {
            b: poly![1.0],
            a: poly![],
        }
        .normalize()
        .unwrap();
    }

    #[test]
    fn test_normalize() {
        let tf = TF {
            b: poly![4.0, 1.0],
            a: poly![8.0, 2.0],
        }
        .normalize()
        .unwrap();
        assert_eq!(tf.b, poly![2.0, 0.5]);
        assert_eq!(tf.a, poly![4.0, 1.0]);
    }

    #[test]
    fn test_from_zpk() {
        assert_eq!(
            TF::from_zpk(ZPK {
                z: vec![complex!(1.0), complex!(2.0)],
                p: vec![complex!(4.0), complex!(8.0)],
                k: complex!(0.5),
            }),
            TF {
                b: poly![1.0, -1.5, 0.5],
                a: poly![32.0, -12.0, 1.0]
            }
        );
    }
}
