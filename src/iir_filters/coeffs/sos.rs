use std::cmp::Ordering;

use anyhow::{bail, ensure, Error, Result};
use nalgebra::ComplexField;
use num::{complex::Complex64, Integer, One, Zero};
use rust_poly::{complex, Get};

use super::{TF, ZPK};

type C64 = Complex64;

#[derive(Default, Clone, PartialEq, Debug)]
pub struct SOSStage {
    pub b0: C64,
    pub b1: C64,
    pub b2: C64,
    pub a0: C64,
    pub a1: C64,
    pub a2: C64,
}

impl SOSStage {
    #[must_use]
    pub fn normalize(&self) -> Self {
        Self {
            b0: self.b0 / self.a0,
            b1: self.b1 / self.a0,
            b2: self.b2 / self.a0,
            a0: complex!(1.),
            a1: self.a1 / self.a0,
            a2: self.a2 / self.a0,
        }
    }
}

#[derive(Clone, Debug)]
pub struct SOS(pub Vec<SOSStage>);

impl TryFrom<ZPK> for SOS {
    type Error = Error;

    fn try_from(value: ZPK) -> std::result::Result<Self, Self::Error> {
        Self::from_zpk(value)
    }
}

impl SOS {
    pub fn from_zpk(
        value: ZPK, /*
                    _pairing: Option<Pairing>,
                    analog: bool,*/
    ) -> Result<Self> {
        let ZPK { mut z, mut p, k } = value;

        if z.is_empty() && p.is_empty() {
            return Ok(Self(vec![SOSStage {
                b0: k,
                b1: C64::zero(),
                b2: C64::zero(),
                a0: C64::one(),
                a1: C64::zero(),
                a2: C64::zero(),
            }]));
        }

        // ensure!(
        //     !false || p.len() >= z.len(),
        //     "for analog zpk2sos conversion, must have p.len() >= z.len()"
        // );

        let n_sections = (p.len().max(z.len()) + 1) / 2;

        // align poles and zeros
        p.extend(vec![C64::zero(); (z.len() - p.len()).max(0)]);
        z.extend(vec![C64::zero(); (p.len() - z.len()).max(0)]);
        if p.len().is_odd() {
            p.push(C64::zero());
            z.push(C64::zero());
        }
        assert_eq!(p.len(), z.len());

        let (zc, zr) = cplxreal(z)?;
        z = [zc, zr].concat();
        let (pc, pr) = cplxreal(p)?;
        p = [pc, pr].concat();
        ensure!(is_real(k), "k must be real");

        let idx_worst = |p: Vec<Complex64>| -> usize {
            arg_min(p.into_iter().map(|x| (1.0 - x.norm()).abs()))
                .expect("infallible")
        };

        let mut sos = vec![SOSStage::default(); n_sections];
        for si in (0..n_sections).rev() {
            let p1 = p.remove(idx_worst(p.clone()));
            sos[si] = if is_real(p1) && !p.iter().any(|x| is_real(*x)) {
                single_zpksos(
                    vec![
                        z.remove(
                            nearest_real_idx(z.clone(), p1).expect("infallible"),
                        ),
                        C64::zero(),
                    ],
                    vec![p1, C64::zero()],
                    C64::one(),
                )?
            } else if p.len() + 1 == z.len()
                && is_real(p1)
                && any_unique(p.iter().copied(), is_real)
                && any_unique(z.iter().copied(), is_real)
            {
                let z1 = z
                    .remove(nearest_complex_idx(z.clone(), p1).expect("infallible"));
                single_zpksos(vec![z1, z1.conj()], vec![p1, p1.conj()], C64::one())?
            } else {
                let p2 = if is_real(p1) {
                    // TODO: explain this magic, there's no fucking way its correct
                    let prealidx: Vec<_> =
                        arg_nonzero(p.iter().map(|&c| u32::from(is_real(c))))
                            .collect();
                    p.remove(
                        prealidx
                            [idx_worst(prealidx.iter().map(|&i| p[i]).collect())],
                    )
                } else {
                    p1.conj()
                };
                if z.is_empty() {
                    single_zpksos(vec![], vec![p1, p2], C64::one())?
                } else {
                    let z1 = z.remove(
                        nearest_complex_or_real_idx(z.clone(), p1)
                            .expect("infallible"),
                    );
                    if !is_real(z1) {
                        single_zpksos(vec![z1, z1.conj()], vec![p1, p2], C64::one())?
                    } else if z.is_empty() {
                        single_zpksos(vec![z1], vec![p1, p2], C64::one())?
                    } else {
                        let z2 = z.remove(
                            nearest_real_idx(z.clone(), p1).expect("infallible"),
                        );
                        assert!(is_real(z2));
                        single_zpksos(vec![z1, z2], vec![p1, p2], C64::one())?
                    }
                }
            }
        }
        assert!(p.is_empty() && z.is_empty());
        sos[0].b0 *= k;
        sos[0].b1 *= k;
        sos[0].b2 *= k;
        Ok(Self(sos))
    }
}

// FIXME: has a bug
fn cplxreal(mut z: Vec<Complex64>) -> Result<(Vec<Complex64>, Vec<Complex64>)> {
    let tol = 100.0 * f64::EPSILON;

    if z.is_empty() {
        return Ok((z.clone(), z));
    }

    // lexicographical sort
    z.sort_by(|a, b| match a.im.abs().total_cmp(&b.im.abs()) {
        Ordering::Equal => a.re.total_cmp(&b.re),
        o => o,
    });

    // split reals and complex values from z into two bins
    let (zr, z): (Vec<_>, Vec<_>) = z
        .into_iter()
        .map(|x| {
            if x.im.abs() <= tol * x.norm() {
                (Some(x), None)
            } else {
                (None, Some(x))
            }
        })
        .unzip();

    let zr: Vec<_> = zr
        .into_iter()
        .flatten()
        .map(|x| complex!(x.re.signum() * x.norm()))
        .collect();

    let z: Vec<_> = z.into_iter().flatten().collect();

    if z.is_empty() {
        return Ok((vec![], zr));
    }

    // split complexes into negative im and positive im
    let mut zp: Vec<_> = z.clone().into_iter().filter(|x| x.im > 0.0).collect();
    let mut zn: Vec<_> = z.into_iter().filter(|x| x.im < 0.0).collect();

    if zp.len() != zn.len() {
        bail!("Vector contains complex value with no matching conjugate.");
    }

    // get indexes of consecutive almost equal values
    let same_real: Vec<_> = diff(zp.clone().into_iter().map(|x| x.re))
        .zip(zp.clone())
        .map(|(a, b)| i32::from(a <= tol * b.norm()))
        .collect();

    // do some index juggling magic to get the starting and stopping indexes of
    // conjugates (see SciPy.signal._cplxreal)
    let diffs: Vec<_> =
        diff(std::iter::once(0).chain(same_real).chain([0])).collect();
    let run_starts: Vec<_> =
        arg_nonzero(diffs.clone().into_iter().map(|x| i32::from(x > 0))).collect();
    let run_stops: Vec<_> =
        arg_nonzero(diffs.into_iter().map(|x| i32::from(x < 0))).collect();

    // sort each run of conjugates by the magnitude of their imaginary axis
    for i in 0..(run_starts).len() {
        let start = run_starts[i];
        let stop = run_stops[i] + 1;
        zp[start..stop].sort_by(|a, b| a.im.abs().total_cmp(&b.im.abs()));
        zn[start..stop].sort_by(|a, b| a.im.abs().total_cmp(&b.im.abs()));
    }

    // assert that they are all conjugates
    if zp
        .clone()
        .into_iter()
        .zip(zn.clone())
        .map(|(xp, xn)| (xp - xn.conj()).abs())
        .zip(zn.clone())
        .any(|(d, xn)| d > tol * xn.abs())
    {
        bail!("Vector contains complex value with no matching conjugate.");
    }

    // complex values are the positive half of each conjugate pair, averaged from
    // the two values in the pair
    let zc: Vec<_> = zp
        .into_iter()
        .zip(zn.clone())
        .map(|(xp, xn)| (xp + xn.conj()) / complex!(2.0))
        .collect();

    Ok((zc, zr))
}

fn diff<T: std::ops::Sub<Output = T>>(
    z: impl Iterator<Item = T> + Clone,
) -> impl Iterator<Item = T> {
    z.clone().zip(z.skip(1)).map(|(a, b)| b - a)
}

fn arg_nonzero<T: num::traits::Zero>(
    z: impl Iterator<Item = T>,
) -> impl Iterator<Item = usize> {
    z.enumerate()
        .filter_map(|(i, x)| if x.is_zero() { None } else { Some(i) })
}

fn arg_min<T: PartialOrd>(z: impl Iterator<Item = T>) -> Option<usize> {
    z.enumerate()
        .min_by(|(_, value0), (_, value1)| partial_cmp_or_eq(value0, value1))
        .map(|(idx, _)| idx)
}

/// returns true if the iterator has a single element that matches the condition
fn any_unique<T>(mut z: impl Iterator<Item = T>, f: impl Fn(T) -> bool) -> bool {
    let at_least_once = z.any(&f);
    at_least_once && !z.all(f)
}

// fn arg_max<T: PartialOrd>(z: impl Iterator<Item = T>) -> Option<usize> {
//     z.enumerate()
//         .max_by(|(_, value0), (_, value1)| partial_cmp_or_eq(value0, value1))
//         .map(|(idx, _)| idx)
// }

fn is_real(c: Complex64) -> bool {
    let tol = 100.0 * f64::EPSILON;
    c.im.abs() <= tol
}

/// Get the next closest complex or real element based on distance
fn nearest_complex_or_real_idx(fro: Vec<Complex64>, to: Complex64) -> Option<usize> {
    arg_min(fro.into_iter().map(|x| (x - to).norm()))
}

/// Get the next closest complex element based on distance
fn nearest_complex_idx(fro: Vec<Complex64>, to: Complex64) -> Option<usize> {
    let mut fro: Vec<_> = fro.into_iter().map(|x| (x, (x - to).norm())).collect();
    fro.sort_by(|(_, r1), (_, r2)| partial_cmp_or_eq(r1, r2));
    fro.into_iter()
        .map(|(a, _)| a)
        .enumerate()
        .find_map(|(i, e)| (!is_real(e)).then_some(i))
}

/// Get the next closest real element based on distance
fn nearest_real_idx(fro: Vec<Complex64>, to: Complex64) -> Option<usize> {
    let mut fro: Vec<_> = fro.into_iter().map(|x| (x, (x - to).norm())).collect();
    fro.sort_by(|(_, r1), (_, r2)| partial_cmp_or_eq(r1, r2));
    fro.into_iter()
        .map(|(a, _)| a)
        .enumerate()
        .find_map(|(i, e)| is_real(e).then_some(i))
}

/// Compares two `PartialOrd` and returns a complete ordering, by replacing None
/// with Equals
fn partial_cmp_or_eq<T: PartialOrd>(a: &T, b: &T) -> Ordering {
    a.partial_cmp(b).unwrap_or(Ordering::Equal)
}

// TODO: this should deffo be a constructor on SOSStage
/// Create one second-order section from up to two zeros and poles
fn single_zpksos(
    z: Vec<Complex64>,
    p: Vec<Complex64>,
    k: Complex64,
) -> Result<SOSStage> {
    let ba = TF::from(ZPK { z, p, k });
    if ba.a.len() > 3 {
        bail!("too many poles to construct a single SOS stage");
    }
    if ba.b.len() > 3 {
        bail!("too many zeros to construct a single SOS stage");
    }
    Ok(SOSStage {
        b0: ba.b.get(2usize).unwrap_or_default(),
        b1: ba.b.get(1usize).unwrap_or_default(),
        b2: ba.b.get(0usize).unwrap_or_default(),
        a0: ba.a.get(2usize).unwrap_or_default(),
        a1: ba.a.get(1usize).unwrap_or_default(),
        a2: ba.a.get(0usize).unwrap_or_default(),
    })
}

#[cfg(test)]
mod test {
    use numeric_constant_traits::{
        Fifteen, Five, Four, Nine, Seven, Ten, Three, Twelve, Twenty, Two,
    };

    use super::*;

    #[test]
    fn test_cplxreal() {
        let a = vec![
            complex!(4.0),
            complex!(3.0),
            complex!(1.0),
            complex!(2.0, -2.0),
            complex!(2.0, 2.0),
            complex!(2.0, -1.0),
            complex!(2.0, 1.0),
            complex!(2.0, -1.0),
            complex!(2.0, 1.0),
            complex!(1.0, 1.0),
            complex!(1.0, -1.0),
        ];
        let (c, r) = cplxreal(a).unwrap();
        assert_eq!(
            c,
            vec![
                complex!(1.0, 1.0),
                complex!(2.0, 1.0),
                complex!(2.0, 1.0),
                complex!(2.0, 2.0),
            ]
        );
        assert_eq!(r, vec![C64::one(), complex!(3.0), complex!(4.0)]);

        let a = cplxreal(vec![complex!(-1.); 4]).unwrap();
        assert_eq!(a, (vec![], vec![complex!(-1.); 4]));
    }

    #[test]
    fn test_single_zpksos() {
        assert_eq!(
            single_zpksos(vec![], vec![], C64::zero()).unwrap(),
            SOSStage {
                b0: C64::zero(),
                b1: C64::zero(),
                b2: C64::zero(),
                a0: C64::zero(),
                a1: C64::zero(),
                a2: C64::one(),
            }
        );

        assert_eq!(
            single_zpksos(vec![], vec![], C64::one()).unwrap(),
            SOSStage {
                b0: C64::zero(),
                b1: C64::zero(),
                b2: C64::one(),
                a0: C64::zero(),
                a1: C64::zero(),
                a2: C64::one(),
            }
        );

        assert_eq!(
            single_zpksos(vec![], vec![], -C64::one()).unwrap(),
            SOSStage {
                b0: C64::zero(),
                b1: C64::zero(),
                b2: -C64::one(),
                a0: C64::zero(),
                a1: C64::zero(),
                a2: C64::one(),
            }
        );

        assert_eq!(
            single_zpksos(vec![C64::zero()], vec![C64::zero()], C64::one()).unwrap(),
            SOSStage {
                b0: C64::zero(),
                b1: C64::one(),
                b2: C64::zero(),
                a0: C64::zero(),
                a1: C64::one(),
                a2: C64::zero(),
            }
        );

        assert_eq!(
            single_zpksos(vec![C64::one()], vec![], C64::one()).unwrap(),
            SOSStage {
                b0: C64::zero(),
                b1: C64::one(),
                b2: -C64::one(),
                a0: C64::zero(),
                a1: C64::zero(),
                a2: C64::one(),
            }
        );

        assert_eq!(
            single_zpksos(
                vec![C64::one(), C64::two()],
                vec![C64::three(), C64::four()],
                C64::five(),
            )
            .unwrap(),
            SOSStage {
                b0: C64::five(),
                b1: -C64::fifteen(),
                b2: C64::ten(),
                a0: C64::one(),
                a1: -C64::seven(),
                a2: C64::twelve(),
            }
        );
    }

    #[test]
    fn test_nearest_complex_or_real_idx() {
        assert_eq!(
            nearest_complex_or_real_idx(
                vec![complex!(0.0, 0.1), complex!(1.0, 1.0), C64::zero()],
                C64::i(),
            ),
            Some(0)
        );
    }

    #[test]
    fn test_nearest_complex_idx() {
        assert_eq!(
            nearest_complex_idx(
                vec![complex!(0.0, 0.1), complex!(1.0, 1.0), C64::zero()],
                C64::i(),
            ),
            Some(0)
        );
    }

    #[test]
    fn test_nearest_real_idx() {
        assert_eq!(
            nearest_real_idx(
                vec![complex!(0.0, 0.1), complex!(1.0, 1.0), C64::zero()],
                C64::i(),
            ),
            Some(2)
        );
    }

    #[test]
    fn test_from_zpk() {
        assert_eq!(
            SOS::try_from(ZPK {
                z: vec![complex!(1.0, 1.0), complex!(1.0, -1.0), C64::three()],
                p: vec![C64::three(), C64::four(), C64::five()],
                k: C64::five(),
            })
            .unwrap()
            .0,
            vec![
                SOSStage {
                    b0: C64::five(),
                    b1: -C64::ten(),
                    b2: C64::ten(),
                    a0: C64::one(),
                    a1: -C64::nine(),
                    a2: C64::twenty(),
                },
                SOSStage {
                    b0: C64::one(),
                    b1: -C64::three(),
                    b2: C64::zero(),
                    a0: C64::one(),
                    a1: -C64::three(),
                    a2: C64::zero(),
                },
            ]
        );

        assert_eq!(
            SOS::try_from(ZPK {
                z: vec![complex!(-1.0); 4],
                p: vec![
                    complex!(0., 0.75),
                    complex!(0., 0.25),
                    complex!(0., -0.25),
                    complex!(0., -0.75)
                ],
                k: complex!(0.1),
            })
            .unwrap()
            .0,
            vec![
                SOSStage {
                    b0: complex!(0.1),
                    b1: complex!(0.2),
                    b2: complex!(0.1),
                    a0: complex!(1.0),
                    a1: complex!(0.0),
                    a2: complex!(0.0625),
                },
                SOSStage {
                    b0: complex!(1.),
                    b1: complex!(2.),
                    b2: complex!(1.),
                    a0: complex!(1.),
                    a1: complex!(0.),
                    a2: complex!(0.5625),
                },
            ]
        );
    }

    #[test]
    fn test_arg_min() {
        assert_eq!(arg_min([3, 1, 2].iter()).unwrap(), 1);
    }
}
