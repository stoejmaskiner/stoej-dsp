use crate::processors::{MonoProcessor, Processor};

use super::coeffs::{SOSStage, SOS, ZPK};

use anyhow::{bail, ensure, Result};
use array_macro::array;
use contracts::contract_trait;

use itertools::Itertools;

#[derive(Clone, Debug)]
pub struct Biquad {
    b0: f32,
    b1: f32,
    b2: f32,
    a1: f32,
    a2: f32,
    z1: f32,
    z2: f32,
}

impl Biquad {
    #[must_use]
    pub fn from_sos_stage(sos: SOSStage) -> Self {
        let mut ret = Self::unity();
        ret.set_coeffs_from_sos_stage(sos);
        ret
    }

    #[must_use]
    pub fn unity() -> Self {
        Self {
            b0: 1.0,
            b1: 0.0,
            b2: 0.0,
            a1: 0.0,
            a2: 0.0,
            z1: 0.0,
            z2: 0.0,
        }
    }

    pub fn set_coeffs_to_unity(&mut self) {
        self.a1 = 0.0;
        self.a2 = 0.0;
        self.b0 = 1.0;
        self.b1 = 0.0;
        self.b2 = 0.0;
    }

    pub fn set_coeffs_from_sos_stage(&mut self, sos: SOSStage) {
        let sos = sos.normalize();
        self.a1 = sos.a1.re as f32;
        self.a2 = sos.a2.re as f32;
        self.b0 = sos.b0.re as f32;
        self.b1 = sos.b1.re as f32;
        self.b2 = sos.b2.re as f32;
    }

    pub(crate) fn step_internal(&mut self, x: f32) -> f32 {
        let y = self.b0.mul_add(x, self.z1);
        self.z1 = self.b1.mul_add(x, -self.a1 * y) + self.z2;
        self.z2 = self.b2.mul_add(x, -self.a2 * y);
        y
    }
}

#[contract_trait]
impl Processor for Biquad {
    const I: usize = 1;

    const O: usize = 1;

    fn process_replacing(&mut self, io: &mut [&mut [f32]]) {
        for i in 0..io[0].len() {
            io[0][i] = self.step_internal(io[0][i]);
        }
    }

    fn process(&mut self, in_: &[&[f32]], out: &mut [&mut [f32]]) {
        for i in 0..in_[0].len() {
            out[0][i] = self.step_internal(in_[0][i]);
        }
    }

    fn step(&mut self, in_: &[f32], out: &mut [f32]) {
        out[0] = self.step_internal(in_[0]);
    }

    fn step_replacing(&mut self, io: &mut [f32]) {
        io[0] = self.step_internal(io[0]);
    }

    fn reset(&mut self) {
        self.z1 = 0.0;
        self.z2 = 0.0;
    }
}

#[derive(Clone, Debug)]
pub struct CascadedBiquad<const STAGES: usize>([Biquad; STAGES])
where
    [(); STAGES]: Sized;

impl<const STAGES: usize> CascadedBiquad<STAGES> {
    pub fn from_zpk(zpk: ZPK) -> Result<Self> {
        let sos = SOS::from_zpk(zpk)?;
        let stages = sos.0.len();
        ensure!(
            stages <= STAGES,
            "the degree of the filter must be at most 2 * STAGES"
        );

        let mut v: Vec<_> = sos.0.into_iter().map(Biquad::from_sos_stage).collect();

        if stages < STAGES {
            for _ in 0..(STAGES - v.len()) {
                v.push(Biquad::unity());
            }
        }

        Ok(Self(v.try_into().expect("infallible")))
    }

    /// Returns a unity gain filter, that is a filter that doesn't do anything.
    ///
    /// Useful in APIs where a filter is expected, but where you'd want to
    /// disable the filtering.
    #[must_use]
    pub fn unity() -> Self {
        Self(array![Biquad::unity(); STAGES])
    }

    pub fn set_coeffs_to_unity(&mut self) {
        for bq in &mut self.0 {
            bq.set_coeffs_to_unity();
        }
    }

    /// Updates coefficients to a new transfer function
    pub fn set_coeffs_from_zpk(&mut self, zpk: ZPK) -> Result<()> {
        let sos = SOS::from_zpk(zpk)?;
        for pair in self.0.iter_mut().zip_longest(sos.0) {
            match pair {
                itertools::EitherOrBoth::Both(bq, coeffs) => {
                    bq.set_coeffs_from_sos_stage(coeffs);
                }
                itertools::EitherOrBoth::Left(bq) => bq.set_coeffs_to_unity(),
                _ => bail!("the degree of the filter must be at most 2 * STAGES"),
            }
        }
        Ok(())
    }
}

#[contract_trait]
impl<const STAGES: usize> Processor for CascadedBiquad<STAGES> {
    const I: usize = 1;
    const O: usize = 1;
    fn process_replacing(&mut self, io: &mut [&mut [f32]]) {
        for i in 0..io[0].len() {
            for s in 0..STAGES {
                io[0][i] = self.0[s].step_internal(io[0][i]);
            }
        }
    }

    fn process(&mut self, in_: &[&[f32]], out: &mut [&mut [f32]]) {
        for i in 0..in_[0].len() {
            for s in 0..STAGES {
                out[0][i] = self.0[s].step_internal(in_[0][i]);
            }
        }
    }

    fn step(&mut self, in_: &[f32], out: &mut [f32]) {
        for s in 0..STAGES {
            out[0] = self.0[s].step_internal(in_[0]);
        }
    }

    fn step_replacing(&mut self, io: &mut [f32]) {
        for s in 0..STAGES {
            io[0] = self.0[s].step_internal(io[0]);
        }
    }

    fn reset(&mut self) {
        for stage in &mut self.0 {
            stage.reset();
        }
    }
}

impl<const STAGES: usize> MonoProcessor for CascadedBiquad<STAGES> {
    fn step_mono(&mut self, x: f32) -> f32 {
        let mut x = x;
        self.0.iter_mut().for_each(|bq| {
            x = bq.step_internal(x);
        });
        x
    }
}

#[cfg(test)]
mod test {

    use crate::iir_filters::design::butter;
    use crate::iir_filters::design::FilterTopology::LowPass;
    use crate::iir_filters::processors::CascadedBiquad;
    use crate::processors::Processor;

    #[test]
    fn biquad() {
        let zpk = butter(4, (150.0, 1.0), LowPass, 2000.0).unwrap();
        let mut bq = CascadedBiquad::<2>::from_zpk(zpk).unwrap();
        let mut data = [1.0; 64];
        let data = &mut [data.as_mut_slice()];
        bq.process_replacing(data);
        //dbg!(data);
    }
}
