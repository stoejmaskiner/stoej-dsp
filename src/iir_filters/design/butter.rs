use std::f64::consts::PI;

use crate::iir_filters::{
    coeffs::ZPK,
    design::{normalize_freq, prewarp_freq},
};
use anyhow::Result;
use num::{complex::Complex64, One};
use rust_poly::complex;

use super::{bilinear_zpk, lp2bp, lp2bs, lp2hp, lp2lp, FilterTopology};

/// Analog prototype for a Butterworth filter
///
/// Port of: https://github.com/scipy/scipy/blob/f990b1d2471748c79bc4260baf8923db0a5248af/scipy/signal/_`filter_design.py#L4322`
#[must_use]
pub fn proto(order: i32) -> ZPK {
    let z = vec![];
    let m: Vec<_> = (-order + 1..order)
        .step_by(2)
        .map(|x| complex!(f64::from(x)))
        .collect();
    let i_pi: Complex64 = Complex64::i() * complex!(PI);
    let p: Vec<_> = m
        .iter()
        .map(|x| -(x * i_pi / (f64::from(order) * 2.)).exp())
        .collect();
    ZPK {
        z,
        p,
        k: Complex64::one(),
    }
}

pub fn butter(
    order: i32,
    critical_freqs: (f64, f64),
    topology: FilterTopology,
    sr: f64,
) -> Result<ZPK> {
    let norm_low_cut = normalize_freq(critical_freqs.0, sr)?;
    let norm_high_cut = normalize_freq(critical_freqs.1, sr)?;
    let proto = proto(order);
    let warp_low_cut = prewarp_freq(norm_low_cut);
    let warp_high_cut = prewarp_freq(norm_high_cut);

    let zpk = match topology {
        FilterTopology::LowPass => lp2lp(proto, warp_low_cut),
        FilterTopology::HighPass => lp2hp(proto, warp_low_cut),
        FilterTopology::BandPass => {
            let band_width = warp_high_cut - warp_low_cut;
            let band_center = (warp_low_cut * warp_high_cut).sqrt();
            lp2bp(proto, band_center, band_width)
        }
        FilterTopology::BandStop => {
            let band_width = warp_high_cut - warp_low_cut;
            let band_center = (warp_low_cut * warp_high_cut).sqrt();
            lp2bs(proto, band_center, band_width)
        }
    };

    Ok(bilinear_zpk(zpk, 2.0))
}

#[cfg(test)]
mod test {
    use num::{complex::Complex64, One};
    use rust_poly::complex as c;

    use super::{butter, proto};
    use crate::{iir_filters::design::FilterTopology, test_utils::complex_vec_eq};

    const TOL: f64 = 0.001;

    #[test]
    fn test_butter_proto() {
        let zpk = proto(4);
        assert_eq!(zpk.z, vec![]);
        assert!(complex_vec_eq(
            &[
                c!(-0.382_683_43, 0.923_879_5),
                c!(-0.923_879_53, 0.382_683_43),
                c!(-0.923_879_53, -0.382_683_43),
                c!(-0.382_683_43, -0.923_879_53)
            ],
            &zpk.p,
            TOL
        ));
        assert_eq!(zpk.k, Complex64::one());
    }

    #[test]
    fn test_butter() {
        let zpk = butter(4, (1.0, 1.0), FilterTopology::LowPass, 4.0).unwrap();
        assert_eq!(zpk.z, vec![c!(-1.); 4]);
        assert!(complex_vec_eq(
            &zpk.p,
            &[
                c!(0.0, 0.668_178_6),
                c!(0.0, 0.198_912_37),
                c!(0.0, -0.198_912_37),
                c!(0.0, -0.668_178_6)
            ],
            TOL
        ));
        assert!((zpk.k - c!(0.093_980_851_433_794_44)).norm() < TOL);
    }
}
