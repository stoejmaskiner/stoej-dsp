use rust_poly::{complex, Poly64};

use crate::{iir_filters::coeffs::ZPK, vec_utils::VecOps};

use super::{
    bilinear_zpk, lp2bp, lp2bs, lp2hp, lp2lp, normalize_freq, prewarp_freq,
    FilterTopology,
};

use anyhow::{Context, Result};

pub fn bessel(
    order: u32,
    critical_freqs: (f64, f64),
    topology: FilterTopology,
    sr: f64,
) -> Result<ZPK> {
    let norm_low_cut = normalize_freq(critical_freqs.0, sr)?;
    let norm_high_cut = normalize_freq(critical_freqs.1, sr)?;
    let proto =
        proto(order).context(format!("Bessel filter order is too high: {order}"))?;
    let warp_low_cut = prewarp_freq(norm_low_cut);
    let warp_high_cut = prewarp_freq(norm_high_cut);

    let zpk = match topology {
        FilterTopology::LowPass => lp2lp(proto, warp_low_cut),
        FilterTopology::HighPass => lp2hp(proto, warp_low_cut),
        FilterTopology::BandPass => {
            let band_width = warp_high_cut - warp_low_cut;
            let band_center = (warp_low_cut * warp_high_cut).sqrt();
            lp2bp(proto, band_center, band_width)
        }
        FilterTopology::BandStop => {
            let band_width = warp_high_cut - warp_low_cut;
            let band_center = (warp_low_cut * warp_high_cut).sqrt();
            lp2bs(proto, band_center, band_width)
        }
    };

    Ok(bilinear_zpk(zpk, 2.0))
}

/// Ref: https://github.com/scipy/scipy/blob/f990b1d2471748c79bc4260baf8923db0a5248af/scipy/signal/_filter_design.py#L4829C5-L4829C5
pub fn proto(order: u32) -> Option<ZPK> {
    let order = order as usize;

    if order == 0 {
        return Some(ZPK {
            z: vec![],
            p: vec![],
            k: complex!(1.),
        });
    }

    let mut p = Poly64::reverse_bessel(order)?.roots();

    // normalize cutoff so 1/2 max phase shift is at 1 rad/sec
    let a_last =
        (falling_factorial(2 * order, order) / 2u128.pow(order as u32)) as f64;
    p.scalar_mut_mul(complex!(10.0f64.powf(-a_last.log10() / order as f64)));

    Some(ZPK {
        z: vec![],
        p,
        k: complex!(1.),
    })
}

/// Ref: https://github.com/scipy/scipy/blob/f990b1d2471748c79bc4260baf8923db0a5248af/scipy/signal/_filter_design.py#L4630
fn falling_factorial(x: usize, n: usize) -> u128 {
    let x = x as u128;
    let n = n as u128;

    let mut val = 1;
    for k in (x - n + 1)..(x + 1) {
        val *= k;
    }
    val
}
