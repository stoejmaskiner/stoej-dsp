use std::f64::consts::PI;

use crate::vec_utils::{VecFuncs, VecMaps, VecOps};

use super::coeffs::ZPK;

use anyhow::{ensure, Result};

use nalgebra as na;
use num::{complex::Complex64, One};

use rust_poly::complex as c;

mod bessel;
mod butter;
pub use bessel::bessel;
pub use butter::butter;
mod cheby1;
mod cheby2;
mod crossover;
mod elliptic;
mod optimum;

#[derive(PartialEq, Eq)]
pub enum FilterTopology {
    LowPass,
    HighPass,
    BandPass,
    BandStop,
}

/// normalize cutoff to (0, 1.0), where 1.0 is nyquist
fn normalize_freq(f: f64, sr: f64) -> Result<f64> {
    let norm_f = 2.0 * f / sr;

    ensure!(
        norm_f < 1.0,
        format!(
            "frequency must be lower than nyquist frequency ({})",
            sr / 2.0
        )
    );

    ensure!(norm_f > 0.0, "frequency must be higher than 0");

    Ok(norm_f)
}

fn prewarp_freq(f: f64) -> f64 {
    // pre-condition: frequency was already normalized
    debug_assert!(f > 0.0 && f < 1.0);
    4.0 * (PI * f / 2.0).tan()
}

/// Convert lowpass prototype to a specific digital lowpass filter
///
/// Ref: https://github.com/scipy/scipy/blob/f990b1d2471748c79bc4260baf8923db0a5248af/scipy/signal/_`filter_design.py#L2695`
fn lp2lp(zpk: ZPK, cut: f64) -> ZPK {
    let degree = relative_degree(&zpk.z, &zpk.p);
    let z_lp: Vec<_> = zpk.z.scalar_mul(c!(cut));
    let p_lp: Vec<_> = zpk.p.scalar_mul(c!(cut));
    let k_lp = zpk.k * cut.powi(i32::try_from(degree).unwrap());
    ZPK {
        z: z_lp.as_slice().to_vec(),
        p: p_lp.as_slice().to_vec(),
        k: k_lp,
    }
}

/// Convert lowpass prototype to a specific digital highpass filter
///
/// Ref: https://github.com/scipy/scipy/blob/f990b1d2471748c79bc4260baf8923db0a5248af/scipy/signal/_`filter_design.py#L2755`
fn lp2hp(zpk: ZPK, cut: f64) -> ZPK {
    let degree = relative_degree(&zpk.z, &zpk.p);
    let z_hp: Vec<_> = zpk.z.el_recip().scalar_mul(c!(cut));
    let p_hp: Vec<_> = zpk.p.el_recip().scalar_mul(c!(cut));
    let z_hp: Vec<_> = [z_hp, vec![c!(0.); degree]].concat();
    let k_hp = zpk.k * zpk.z.map(|x| -x).iter().product::<Complex64>()
        / zpk.p.iter().map(|x| -x).product::<Complex64>();
    ZPK {
        z: z_hp,
        p: p_hp.as_slice().into(),
        k: k_hp,
    }
}

/// Convert lowpass prototype to a specific digital bandpass filter
///
/// Ref: https://github.com/scipy/scipy/blob/f990b1d2471748c79bc4260baf8923db0a5248af/scipy/signal/_filter_design.py#L2821-L2897
fn lp2bp(zpk: ZPK, cut: f64, width: f64) -> ZPK {
    let degree = relative_degree(&zpk.z, &zpk.p);
    let z_bp: Vec<_> = zpk.z.scalar_mul(c!(width / 2.));
    let p_bp: Vec<_> = zpk.p.scalar_mul(c!(width / 2.));

    let aux: Vec<_> = z_bp.powi(2).scalar_add(-c!(cut.powi(2))).sqrt();
    let z_bp: Vec<_> = [z_bp.el_add(&aux), z_bp.el_sub(&aux)].concat();

    let aux: Vec<_> = p_bp.powi(2).scalar_add(-c!(cut.powi(2))).sqrt();
    let p_bp: Vec<_> = [p_bp.el_add(&aux), p_bp.el_sub(&aux)].concat();

    let z_bp: Vec<_> = [z_bp, vec![c!(0.); degree]].concat();

    let k_bp = zpk.k * width.powi(degree.try_into().unwrap());
    ZPK {
        z: z_bp,
        p: p_bp,
        k: k_bp,
    }
}

/// Convert lowpass prototype to a specific digital bandstop filter
///
/// Ref: https://github.com/scipy/scipy/blob/f990b1d2471748c79bc4260baf8923db0a5248af/scipy/signal/_filter_design.py#L2900C7-L2900C7
fn lp2bs(zpk: ZPK, cut: f64, width: f64) -> ZPK {
    let degree = relative_degree(&zpk.z, &zpk.p);
    let z_bs: Vec<_> = zpk.z.el_recip().scalar_mul(c!(width / 2.));
    let p_bs: Vec<_> = zpk.p.el_recip().scalar_mul(c!(width / 2.));

    let aux: Vec<_> = z_bs.powi(2).scalar_add(-c!(cut.powi(2))).sqrt();
    let z_bs: Vec<_> = [z_bs.el_add(&aux), z_bs.el_sub(&aux)].concat();

    let aux: Vec<_> = p_bs.powi(2).scalar_add(-c!(cut.powi(2))).sqrt();
    let p_bs: Vec<_> = [p_bs.el_add(&aux), p_bs.el_sub(&aux)].concat();

    let z_bs: Vec<_> = [
        z_bs,
        vec![c!(cut) * Complex64::i(); degree],
        vec![c!(cut) * -Complex64::i(); degree],
    ]
    .concat();

    let k_bs = zpk.k
        * ((-na::DVector::from(zpk.z)).product()
            / (-na::DVector::from(zpk.p)).product())
        .re;

    ZPK {
        z: z_bs,
        p: p_bs,
        k: k_bs,
    }
}

fn relative_degree(z: &Vec<Complex64>, p: &Vec<Complex64>) -> usize {
    p.len()
        .checked_sub(z.len())
        .expect("Transfer function must have at least as many poles as zeros.")
}

/// Transform an analog filter to a digital one using a bilinear transformation
///
/// Ref: https://github.com/scipy/scipy/blob/f990b1d2471748c79bc4260baf8923db0a5248af/scipy/signal/_filter_design.py#L2613C9-L2613C9
fn bilinear_zpk(zpk: ZPK, sr: f64) -> ZPK {
    let degree = relative_degree(&zpk.z, &zpk.p);
    let sr2 = 2.0 * sr;

    // bilinear transform
    let z_z: Vec<_> = zpk
        .z
        .iter()
        .map(|z| z + sr2)
        .zip(zpk.z.iter().map(|z| -z + sr2))
        .map(|(a, b)| a / b)
        .collect();
    let p_z = zpk
        .p
        .iter()
        .map(|p| p + sr2)
        .zip(zpk.p.iter().map(|p| -p + sr2))
        .map(|(a, b)| a / b)
        .collect();

    // zeros at infinity get moved to Nyquist
    let z_z: Vec<_> = [z_z, vec![-Complex64::one(); degree]].concat();

    // compensate gain
    let k_z = zpk.k
        * (zpk.z.iter().map(|z| sr2 - z).product::<Complex64>()
            / zpk.p.iter().map(|p| sr2 - p).product::<Complex64>())
        .re;

    ZPK {
        z: z_z,
        p: p_z,
        k: k_z,
    }
}

#[cfg(test)]
mod test {

    use std::f64::consts::SQRT_2;

    use rust_poly::complex as c;

    use super::{bilinear_zpk, lp2lp};
    use crate::{iir_filters::design::butter::proto, test_utils::complex_vec_eq};

    const TOL: f64 = 0.001;

    #[test]
    fn test_lp2lp() {
        let zpk = lp2lp(proto(2), 2.0);
        assert_eq!(zpk.z, vec![]);
        assert!(complex_vec_eq(
            &zpk.p,
            &[c!(-SQRT_2, SQRT_2), c!(-SQRT_2, -SQRT_2)],
            TOL
        ));
        assert_eq!(zpk.k, c!(4.0));
    }

    #[test]
    fn test_bilinear_zpk() {
        let zpk = bilinear_zpk(lp2lp(proto(2), 2.0), 4.0);
        assert_eq!(zpk.z, vec![c!(-1.), c!(-1.)]);
        assert!(complex_vec_eq(
            &zpk.p,
            &[c!(0.662_051_3, 0.249_675_18), c!(0.662_051_3, -0.249_675_1)],
            TOL
        ));
        assert!((zpk.k - c!(0.044_136_753_893_025_75)).norm() < TOL);
    }
}
