use std::fmt::Display;

use array_macro::array;
use fastrand as fr;
use weird_data as wd;

use crate::const_utils::__DummyTrait;

mod convolver;
mod delay;
mod queue;
mod stack;

pub use convolver::*;
pub use delay::*;
pub use queue::*;
pub use stack::*;

/// A fast bare-bones ring buffer with a fixed size. Used as the base to
/// implement other data structures such as delay lines, stacks and queues.
///
/// It is basically a small array, but instead of doing bounds checks, panicking
/// or having undefined behavior when an index is out of bounds, it wraps it
/// around. This is much faster, and allows for some convenient circular
/// indexing without having to use modulo or any conditionals. No matter the
/// index used, it is always well-behaved and can index without any branches.
///
/// Size must be a power of 2, and is configured by providing
/// the exponent of the power `E`. For example, to get a delay line of
/// size 1024, provide `E = 10`.
#[derive(Debug, Clone)]
pub struct Base<const E: usize>([f32; 1 << E])
where
    [(); 1 << E]: __DummyTrait;

impl<const E: usize> Base<E>
where
    [(); 1 << E]: __DummyTrait,
{
    pub fn new() -> Self {
        Self(array![0.0; 1 << E])
    }

    // TODO(bench): #[inline]
    fn wrap_idx(idx: usize) -> usize {
        idx & ((1 << E) - 1)
    }

    /// A very fast branchless get method, that wraps the index using bit magic.
    /// Because length is always a power of two, we can exploit the AND operator
    /// to clamp the index by keeping the lowest E bits.
    pub fn get_branchless(&self, idx: usize) -> f32 {
        let idx = Self::wrap_idx(idx);
        // TODO(proof): formally prove that this never happens
        debug_assert!(idx < self.0.len());
        unsafe { *self.0.get_unchecked(idx) }
    }

    /// A very fast branchess set method, that wraps the index using bit magic.
    /// Because length is always a power of two, we can exploit the AND operator
    /// to clamp the index by keeping the lowest E bits.
    pub fn set_branchless(&mut self, idx: usize, val: f32) {
        let idx = Self::wrap_idx(idx);
        // TODO(proof): formally prove that this never happens
        debug_assert!(idx < self.0.len());
        *unsafe { self.0.get_unchecked_mut(idx) } = val;
    }

    pub fn as_slice(&self) -> &[f32] {
        return self.0.as_slice();
    }

    pub fn as_mut_slice(&mut self) -> &mut [f32] {
        return self.0.as_mut_slice();
    }

    pub fn capacity(&self) -> usize {
        return self.0.len();
    }

    pub fn clear(&mut self) {
        self.0.fill(0.0);
    }

    pub fn fill(&mut self, val: f32) {
        self.0.fill(val);
    }
}

impl<const E: usize> Default for Base<E>
where
    [(); 1 << E]: __DummyTrait,
{
    fn default() -> Self {
        return Self::new();
    }
}

#[cfg(test)]
mod test_unit {
    use super::*;

    #[test]
    fn new_capacity() {
        let buff = Base::<2>::new();
        assert_eq!(buff.capacity(), 4);
        assert_eq!(buff.get_branchless(0), 0.0);
    }

    #[test]
    fn get_set() {
        let mut buff = Base::<2>::new();
        buff.set_branchless(0, 1.0);
        buff.set_branchless(3, 2.0);
        assert_eq!(buff.get_branchless(0), 1.0);
        assert_eq!(buff.get_branchless(3), 2.0);
    }

    #[test]
    fn set_get_wrapping() {
        let mut buff = Base::<2>::new();
        buff.set_branchless(1, 1.0);
        assert_eq!(buff.get_branchless(9), 1.0);
    }

    #[test]
    fn set_wrapping_get() {
        let mut buff = Base::<2>::new();
        buff.set_branchless(9, 1.0);
        assert_eq!(buff.get_branchless(1), 1.0);
    }

    #[test]
    fn set_clear() {
        let mut buff = Base::<2>::new();
        buff.set_branchless(1, 1.0);
        buff.clear();
        assert_eq!(buff.get_branchless(1), 0.0);
    }
}

#[cfg(test)]
mod test_fuzz {
    use super::*;

    #[test]
    #[ignore]
    fn max_size_all_ops() {
        // very big (biggest that doesn't overflow the stack)
        let mut b_big: Base<14> = Base::new();

        for _ in 0..(1 << 20) {
            let idx = wd::usize();
            match fr::u8(0..4) {
                0 => b_big.set_branchless(idx, wd::special_f32()),
                1 => {
                    b_big.get_branchless(idx);
                }
                2 => b_big.clear(),
                3 => b_big.fill(wd::special_f32()),
                _ => unreachable!(),
            }
        }
    }

    #[test]
    #[ignore]
    fn min_size_all_ops() {
        // smallest
        let mut b_small: Base<0> = Base::new();

        for _ in 0..(1 << 20) {
            let idx = wd::usize();
            match fr::u8(0..4) {
                0 => b_small.set_branchless(idx, wd::special_f32()),
                1 => {
                    b_small.get_branchless(idx);
                }
                2 => b_small.clear(),
                3 => b_small.fill(wd::special_f32()),
                _ => unreachable!(),
            }
        }
    }
}
