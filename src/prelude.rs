pub use crate::processors::{
    basics::{ProcAdd, ProcDiv, ProcGain, ProcMul, ProcSub},
    combinators::ProcParallel,
    traits::Processor,
};
