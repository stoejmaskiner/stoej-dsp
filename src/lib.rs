#![doc = include_str!("../README.md")]
#![warn(clippy::pedantic, clippy::nursery)]
#![allow(clippy::cast_possible_truncation)]
#![allow(clippy::tuple_array_conversions)]
#![allow(incomplete_features)]
#![feature(generic_const_exprs)]
#![feature(concat_idents)]

pub mod buffers;
pub mod const_utils;
pub mod iir_filters;
pub mod oversampling;
pub mod prelude;
pub mod processors;
pub mod ring_buffer;
mod validation_utils;
mod vec_utils;

#[cfg(feature = "graph")]
pub use stoej_dsp_graph as graph;

#[cfg(test)]
mod test_utils;
