//! Generic audio buffer traits and concrete buffers that implement them.
//!
//! The Buffer API is implemented for STOEJ-DSP [FlatBuffer]s and [BufferView]s,
//! or to external types such as
//! [nih_plug](https://github.com/robbert-vdh/nih-plug/tree/master/src)'s own
//! buffer type, slices, arrays and `Vec`. Converting to [FlatBuffer] may or may
//! not give performance advantages, so make sure to benchmark. Either way, the
//! [Processor] API is agnostic to which backing storage you use, as long as it
//! implements the [Buffer] traits.
//!
//! Make sure to enable the `nih_plug` feature flag in order to use the adaptors
//! for [nih_plug](https://github.com/robbert-vdh/nih-plug/tree/master/src) buffers.

use std::ops::{Index, IndexMut};

/// Generic collection trait suitable for buffers
///
/// Consider implementing [Buffer] instead
pub trait BufferStorage: Index<usize, Output = f32> + IndexMut<usize> {
    fn iter(&self) -> impl Iterator<Item = &f32>;

    fn iter_mut(&mut self) -> impl Iterator<Item = &mut f32>;

    fn len(&self) -> usize;

    /// Fills buffer from an iterator. Works even if the lengths don't match.
    ///
    /// If the iterator is longer, the remainder is ignored. If the iterator is
    /// shorter, the rest of the buffer is garbage.
    ///
    /// Note that the default implementation tries to avoid bounds checking,
    /// but you can probably do better by overriding the implementation.
    fn fill_from_iter(&mut self, iter: impl Iterator<Item = f32>) {
        self.iter_mut().zip(iter).for_each(|(lhs, rhs)| {
            *lhs = rhs;
        });
    }
}

/// Zero-allocation replacing and non-replacing operations on buffers
pub trait Buffer: BufferStorage {
    fn add_mut(&mut self, other: &Self) {
        // hopefully this gets vectorized, please check when implementing
        for (lhs, rhs) in self.iter_mut().zip(other.iter()) {
            *lhs += rhs;
        }
    }

    fn sub_mut(&mut self, other: &Self) {
        for (lhs, rhs) in self.iter_mut().zip(other.iter()) {
            *lhs -= rhs;
        }
    }

    fn mul_mut(&mut self, other: &Self) {
        for (lhs, rhs) in self.iter_mut().zip(other.iter()) {
            *lhs *= rhs;
        }
    }

    fn div_mut(&mut self, other: &Self) {
        for (lhs, rhs) in self.iter_mut().zip(other.iter()) {
            *lhs /= rhs;
        }
    }

    /// Calculate the negative of self
    fn neg_mut(&mut self) {
        for x in self.iter_mut() {
            *x = -*x;
        }
    }

    /// Calculate the reciprocal of self
    fn recip_mut(&mut self) {
        for x in self.iter_mut() {
            *x = 1.0 / *x;
        }
    }

    /// Calculate 1 - self (probability complement)
    fn complement_mut(&mut self) {
        for x in self.iter_mut() {
            *x = 1.0 - *x;
        }
    }

    fn add_copy(&mut self, lhs: &Self, rhs: &Self) {
        for ((dst, lhs), rhs) in self.iter_mut().zip(lhs.iter()).zip(rhs.iter()) {
            *dst = lhs + rhs;
        }
    }

    fn sub_copy(&mut self, lhs: &Self, rhs: &Self) {
        for ((dst, lhs), rhs) in self.iter_mut().zip(lhs.iter()).zip(rhs.iter()) {
            *dst = lhs - rhs;
        }
    }

    fn mul_copy(&mut self, lhs: &Self, rhs: &Self) {
        for ((dst, lhs), rhs) in self.iter_mut().zip(lhs.iter()).zip(rhs.iter()) {
            *dst = lhs * rhs;
        }
    }

    fn div_copy(&mut self, lhs: &Self, rhs: &Self) {
        for ((dst, lhs), rhs) in self.iter_mut().zip(lhs.iter()).zip(rhs.iter()) {
            *dst = lhs / rhs;
        }
    }
}
