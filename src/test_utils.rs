use num::complex::Complex64;

pub fn complex_vec_eq(av: &[Complex64], bv: &[Complex64], tol: f64) -> bool {
    av.iter()
        .zip(bv.iter())
        .map(|(a, b)| (a - b).norm())
        .all(|x| x < tol)
}

macro_rules! macro_num {
    () => {
        0
    };

    (1) => {
        1
    };

    (1 $($rest:tt)*) => {
        1 + macro_num!($($rest)*)
    }
}
