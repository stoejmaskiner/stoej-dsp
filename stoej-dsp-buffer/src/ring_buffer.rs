use array_macro::array;

mod queue;
mod stack;

pub use queue::*;
pub use stack::*;

mod sealed {
    pub trait Sealed {}
}

pub struct BufferLen<const N: usize>;

pub trait SupportedBufferLen: sealed::Sealed {}

macro_rules! impl_supported_buffer_len {
    ($val:literal) => {
        impl sealed::Sealed for BufferLen<$val> {}
        impl SupportedBufferLen for BufferLen<$val> {}
    };
}

impl_supported_buffer_len!(1);
impl_supported_buffer_len!(2);
impl_supported_buffer_len!(4);
impl_supported_buffer_len!(8);
impl_supported_buffer_len!(16);
impl_supported_buffer_len!(32);
impl_supported_buffer_len!(64);
impl_supported_buffer_len!(128);
impl_supported_buffer_len!(256);
impl_supported_buffer_len!(512);
impl_supported_buffer_len!(1024);
impl_supported_buffer_len!(2048);
impl_supported_buffer_len!(4096);
impl_supported_buffer_len!(8192);
impl_supported_buffer_len!(16384);
impl_supported_buffer_len!(32768);
impl_supported_buffer_len!(65536);
impl_supported_buffer_len!(131072);
impl_supported_buffer_len!(262144);
impl_supported_buffer_len!(524288);
impl_supported_buffer_len!(1048576);
impl_supported_buffer_len!(2097152);
impl_supported_buffer_len!(4194304);
impl_supported_buffer_len!(8388608);
impl_supported_buffer_len!(16777216);
impl_supported_buffer_len!(33554432);
impl_supported_buffer_len!(67108864);
impl_supported_buffer_len!(134217728);
impl_supported_buffer_len!(268435456);
impl_supported_buffer_len!(536870912);
impl_supported_buffer_len!(1073741824);
impl_supported_buffer_len!(2147483648);

#[derive(Clone, Debug)]
pub struct Base<T, const N: usize>([T; N])
where
    BufferLen<N>: SupportedBufferLen;

impl<T: Default + Clone, const N: usize> Base<T, N>
where
    BufferLen<N>: SupportedBufferLen,
{
    pub fn new() -> Self {
        Self(array![T::default(); N])
    }

    pub fn wrap_idx(idx: usize) -> usize {
        idx & (N - 1)
    }

    /// A very fast branchless get method, that wraps the index using bit magic.
    /// Because length is always a power of two, we can exploit the AND operator
    /// to clamp the index by keeping the lowest `log2(N)` bits
    pub fn get_branchless(&self, idx: usize) -> &T {
        let idx = Self::wrap_idx(idx);
        // TODO: formal proof
        debug_assert!(idx < self.0.len());
        unsafe { self.0.get_unchecked(idx) }
    }

    /// A very fast branchess set method, that wraps the index using bit magic.
    /// Because length is always a power of two, we can exploit the AND operator
    /// to clamp the index by keeping the lowest `log2(N)` bits
    pub fn set_branchless(&mut self, idx: usize, val: T) {
        let idx = Self::wrap_idx(idx);
        // TODO: formal proof
        debug_assert!(idx < self.0.len());
        *unsafe { self.0.get_unchecked_mut(idx) } = val;
    }

    pub fn as_slice(&self) -> &[T] {
        return self.0.as_slice();
    }

    pub fn as_mut_slice(&mut self) -> &mut [T] {
        return self.0.as_mut_slice();
    }

    pub fn capacity(&self) -> usize {
        return self.0.len();
    }

    pub fn clear(&mut self) {
        self.0.fill(T::default());
    }

    pub fn fill(&mut self, val: T) {
        self.0.fill(val);
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn set_get() {
        let mut buf = Base::<f32, 4>::new();
        buf.set_branchless(1, 1.0);
        buf.set_branchless(4, 2.0);
        assert_eq!(*buf.get_branchless(5), 1.0);
        assert_eq!(*buf.get_branchless(0), 2.0);
    }
}
